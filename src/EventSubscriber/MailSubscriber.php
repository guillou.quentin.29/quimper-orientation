<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use JetBrains\PhpStorm\ArrayShape;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final class MailSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly Environment $twig,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function onUserRequestPasswordSuccess(GenericEvent $event): void
    {
        $subject = 'Réinitialisation du mot de passe';
        $to = $event->getSubject()->getEmail();
        try {
            $body = $this->twig->render(
                'emails/request_password.html.twig',
                [
                'user' => $event->getSubject(),
                ]
            );
            $this->sendEmail($subject, $to, $body);
        } catch (LoaderError | RuntimeError | SyntaxError) {
        }
    }

    private function sendEmail($subject, $to, $body): void
    {
        $email = new Email();
        $email
            ->getHeaders()
            ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply');

        $email
            ->from('no-reply@quimper-orientation.fr')
            ->to($to)
            ->subject($subject)
            ->html($body);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->critical(sprintf('Unable to send email: %s', $e->getMessage()));
        }
    }

    #[ArrayShape([
        'security.reset_password' => "string",
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            'security.reset_password' => 'onUserRequestPasswordSuccess',
        ];
    }
}

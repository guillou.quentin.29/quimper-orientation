<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Map;
use JetBrains\PhpStorm\ArrayShape;
use PiWeb\PiCRUD\Event\PiCrudEvents;
use PiWeb\PiCRUD\Event\QueryResultEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class QueryResultEventSubscriber implements EventSubscriberInterface
{
    public function postListQueryResult(QueryResultEvent $event): void
    {
        $results = [];

        /** @var array $results */
        $entities = $event->getResults();
        if ('map' === $event->getType()) {
            /** @var Map $entity */
            foreach ($entities as $entity) {
                $results[$entity->getCity()][] = $entity;
            }

            $event->setResults($results);
        }
    }

    #[ArrayShape([
        PiCrudEvents::POST_LIST_QUERY_RESULT => "string[]",
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            PiCrudEvents::POST_LIST_QUERY_RESULT => ['postListQueryResult'],
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use JetBrains\PhpStorm\ArrayShape;
use PiWeb\PiCRUD\Event\FilterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use PiWeb\PiCRUD\Event\PiCrudEvents;

final class FilterEventSubscriber implements EventSubscriberInterface
{
    public function postFilterQueryBuilder(FilterEvent $event): void
    {
        $composite = $event->getComposite();

        switch ($event->getType()) {
            case 'event':
                if ($event->getName() === 'dateBegin') {
                    $expr = $event->getQueryBuilder()->expr();
                    $composite = $expr->orX(
                      'entity.dateBegin > :' . $event->getName(),
                      'entity.dateEnd > entity.dateBegin AND entity.dateEnd > :' . $event->getName()
                    );
                }
                break;
        }

        $event->setComposite($composite);
    }

    #[ArrayShape([
        PiCrudEvents::POST_FILTER_QUERY_BUILDER => "string[]",
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            PiCrudEvents::POST_FILTER_QUERY_BUILDER => ['postFilterQueryBuilder'],
        ];
    }
}

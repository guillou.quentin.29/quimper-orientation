<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Document;
use App\Entity\Event;
use App\Entity\Menu;
use App\Entity\People;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use JetBrains\PhpStorm\ArrayShape;
use PiWeb\PiCRUD\Event\EntityEvent;
use PiWeb\PiCRUD\Event\PiCrudEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

final class EntityEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly string $rootDir,
    ) {
    }

    public function onEntityPrePersist(GenericEvent $event): void
    {
        $entity = $event->getSubject();

        $entity->setCreateBy($this->security->getUser());
        $entity->setUpdateBy($this->security->getUser());
    }

    public function onEntityPostPersist(GenericEvent $event): void
    {
        $entity = $event->getSubject();

        $this->requestStack->getSession()->getFlashBag()->add('success', $entity . ' a été créé');
    }

    public function onEntityPreUpdate(GenericEvent $event): void
    {
        $entity = $event->getSubject();
        if (method_exists($entity, 'setUpdateBy')) {
            $entity->setUpdateBy($this->security->getUser());
        }
    }

    public function onEntityPostUpdate(GenericEvent $event): void
    {
        $entity = $event->getSubject();

        $this->requestStack->getSession()->getFlashBag()->add('success', $entity . ' a été modifié');
    }

    public function onEntityPostCreate(EntityEvent $event): void
    {
        switch ($event->getType()) {
            case 'people':
                if (isset($event->getOptions()['event'])) {
                    $entity = $this->em->getRepository(Event::class)->findOneById($event->getOptions()['event']);
                    $event->getSubject()->setEvent($entity);
                }
                break;
            case 'team':
                if (isset($event->getOptions()['event'])) {
                    $entity = $this->em->getRepository(Event::class)->findOneById($event->getOptions()['event']);
                    $event->getSubject()->setEvent($entity);

                    for ($i = 0; $i < $entity->getNumberPeopleByEntries(); $i++) {
                        $people = new People();
                        $people->setEvent($entity);

                        $event->getSubject()->addPeople($people);
                    }
                }
        }
    }

    public function onEntityPreRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if ($entity instanceof Document) {
            unlink(sprintf('%s/public/files/%s', $this->rootDir, $entity->getFilename()));
        }

        if ($entity instanceof Menu && !$entity->getChilds()->isEmpty()) {
            foreach ($entity->getChilds() as $child) {
                $this->em->remove($child);
                $this->em->flush();
            }
        }
    }

    #[ArrayShape([
        PiCrudEvents::PRE_ENTITY_PERSIST => "string",
        PiCrudEvents::PRE_ENTITY_UPDATE => "string",
        PiCrudEvents::POST_ENTITY_PERSIST => "string",
        PiCrudEvents::POST_ENTITY_UPDATE => "string",
        PiCrudEvents::POST_ENTITY_CREATE => "string",
        Events::preRemove => "string"
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            PiCrudEvents::PRE_ENTITY_PERSIST => 'onEntityPrePersist',
            PiCrudEvents::PRE_ENTITY_UPDATE => 'onEntityPreUpdate',
            PiCrudEvents::POST_ENTITY_PERSIST => 'onEntityPostPersist',
            PiCrudEvents::POST_ENTITY_UPDATE => 'onEntityPostUpdate',
            PiCrudEvents::POST_ENTITY_CREATE => 'onEntityPostCreate',
            Events::preRemove => 'onEntityPreRemove',
        ];
    }
}

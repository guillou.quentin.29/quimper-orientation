<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use PiWeb\PiCRUD\Event\QueryEvent;
use PiWeb\PiCRUD\Event\PiCrudEvents;

final class QueryEventSubscriber implements EventSubscriberInterface
{
    public function postListQueryBuilder(QueryEvent $event): void
    {
        $queryBuilder = $event->getQueryBuilder();

        switch ($event->getType()) {
            case 'event':
                $queryBuilder->orderBy('entity.dateBegin', 'ASC');
                break;
            case 'result':
                $queryBuilder->join('entity.event', 'event');
                $queryBuilder->orderBy('event.dateBegin', 'DESC');
                break;
            case 'news':
                $queryBuilder
                    ->andWhere('entity.promote = true')
                    ->orderBy('entity.updateAt', 'DESC');
                break;
            case 'map':
                $queryBuilder->orderBy('entity.city, entity.title', 'ASC');
                break;
            default:
                $queryBuilder->orderBy('entity.updateAt', 'DESC');
        }

        $event->setQueryBuilder($queryBuilder);
    }

    public function postAdminQueryBuilder(QueryEvent $event): void
    {
        $queryBuilder = $event->getQueryBuilder();

        switch ($event->getType()) {
            case 'user':
                $queryBuilder->orderBy('entity.email', 'ASC');
                break;
            case 'event':
                $queryBuilder->orderBy('entity.dateBegin', 'DESC');
                break;
            default:
                $queryBuilder->orderBy('entity.updateAt', 'DESC');
        }
    }

    #[ArrayShape([
        PiCrudEvents::POST_LIST_QUERY_BUILDER => "string[]",
        PiCrudEvents::POST_ADMIN_QUERY_BUILDER => "string[]"
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            PiCrudEvents::POST_LIST_QUERY_BUILDER => ['postListQueryBuilder'],
            PiCrudEvents::POST_ADMIN_QUERY_BUILDER => ['postAdminQueryBuilder'],
        ];
    }
}

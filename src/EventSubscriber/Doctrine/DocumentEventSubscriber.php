<?php

declare(strict_types=1);

namespace App\EventSubscriber\Doctrine;

use App\Entity\Document;
use Doctrine\ORM\Event\LifecycleEventArgs;

final class DocumentEventSubscriber
{
    public function __construct(
        private readonly string $rootDir,
    ) {
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        /** @var Document $entity */
        $entity = $args->getObject();
        unlink(sprintf('%s/public/files/%s', $this->rootDir, $entity->getFilename()));
    }
}

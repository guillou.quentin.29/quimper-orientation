<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Entity\Event;
use App\Entity\People;
use DateTime;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use PiWeb\PiCRUD\Event\FormEvent;
use PiWeb\PiCRUD\Event\PiCrudEvents;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use App\Entity\Circuit;

final class FormEventSubscriber implements EventSubscriberInterface
{
    public function postFormBuilderAdd(FormEvent $event): void
    {
        $builder = $event->getBuilder();

        if (!empty($event->getProperties()->type) && $event->getProperties()->type !== 'default') {
            $builder->remove($event->getField());
        }

        switch ($event->getProperties()->type) {
            case 'email':
                $options = $event->getOptions();
                $people = $options['data'] instanceof People ? $options['data'] : null;

                $builder->add('email', EmailType::class, [
                    'required' => $people?->getEvent()?->getId() === 381,
                ]);
                break;
            case 'file':
                $builder->add('file', $event->getProperties()->options['entry_type']);
                break;
            case 'circuits':
            case 'files':
            case 'peoples':
                $builder->add($event->getField(), CollectionType::class, [
                    'entry_type' => $event->getProperties()->options['entry_type'],
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                ]);
                break;
            case 'image':
                $builder->add('imageFile', VichImageType::class, [
                    'label' => 'Image',
                    'required' => false,
                    'allow_delete' => true,
                    'delete_label' => 'Supprimer l\'image',
                    'download_uri' => false,
                    'image_uri' => true,
                    'imagine_pattern' => 'thumbnail',
                    'asset_helper' => true,
                ]);
                break;
            case 'ckeditor':
                $builder->add('content', CKEditorType::class, [
                    'config_name' => 'default',
                    'required' => false,
                ]);
                break;
            case 'choices':
                $builder->add($event->getField(), ChoiceType::class, [
                    'label' => $event->getProperties()->label,
                    'multiple' => true,
                    'choices' => $event->getProperties()->form['choices']
                ]);
                break;
            case 'circuit':
                $options = $event->getOptions();
                $entityEvent = $options['data']->getEvent();

                if (!$entityEvent->getCircuits()->isEmpty()) {
                    $builder->add('circuit', EntityType::class, [
                        'label' => $event->getProperties()->label,
                        'class' => Circuit::class,
                        'query_builder' => fn(EntityRepository $er) => $er->createQueryBuilder('c')
                            ->where('c.event = :event')
                            ->setParameter('event', $options['data']->getEvent()->getId()),
                        'choice_label' => 'label',
                    ]);
                }
                break;
            case 'datetime':
                $builder->add($event->getField(), DateTimeType::class);
                break;
            case 'checkbox':
                $builder->add($event->getField());
                break;
            case 'events':
                $builder->add('events', EntityType::class, [
                    'label' => $event->getProperties()->label,
                    'class' => Event::class,
                    'query_builder' => fn(EntityRepository $er) => $er->createQueryBuilder('e')
                        ->andWhere('e.dateBegin >= :now')
                        ->setParameter('now', new DateTime())
                        ->orderBy('e.dateBegin', 'ASC'),
                    'choice_label' => fn(Event $event) => sprintf(
                        '%s - %s',
                        $event->getDateBegin()->format('d/m/Y'),
                        $event->getTitle()
                    ),
                    'multiple' => true,
                    'required' => false,
                ]);
                break;
        }

        if ('event' === $event->getField()) {
            $builder->add($event->getField(), EntityType::class, [
                'class' => Event::class,
                'query_builder' => fn(EntityRepository $er) => $er->createQueryBuilder('e')
                    ->andWhere('e.dateBegin <= :now')
                    ->setParameter('now', new DateTime())
                    ->orderBy('e.dateBegin', 'DESC'),
                'choice_label' => fn(Event $event) => sprintf(
                    '%s - %s',
                    $event->getDateBegin()->format('d/m/Y'),
                    $event->getTitle()
                ),
            ]);
        }
    }

    public function postSearchBuilderAdd(FormEvent $event): void
    {
        $builder = $event->getBuilder();

        if (!empty($event->getProperties()->type) && $event->getProperties()->type !== 'default') {
            $builder->remove($event->getField());
        }

        switch ($event->getProperties()->type) {
            case 'datetime':
                $builder->add($event->getField(), DateType::class, [
                  'attr' => ['operator' => '>='],
                  'required' => false,
                ]);
                break;
        }
    }

    #[ArrayShape([
        PiCrudEvents::POST_FORM_BUILDER_ADD => "string[]",
        PiCrudEvents::POST_SEARCH_BUILDER_ADD => "string[]",
    ])]
    public static function getSubscribedEvents(): array
    {
        return [
            PiCrudEvents::POST_FORM_BUILDER_ADD => ['postFormBuilderAdd'],
            PiCrudEvents::POST_SEARCH_BUILDER_ADD => ['postSearchBuilderAdd'],
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
final class Entry
{
    /**
     * @Required
     */
    public string $name;

    public function getName(): string
    {
        return $this->name;
    }
}

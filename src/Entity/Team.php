<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Model\IdTrait;
use App\Model\LabelTrait;
use App\Model\EventReferenceTrait;
use JetBrains\PhpStorm\Pure;
use PiWeb\PiCRUD\Annotation as PiCRUD;
use PiWeb\PiCRUD\Model\AuthorTrait;

/**
 * @PiCRUD\Entity(
 *      name="team",
 * )
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Team
{
    use IdTrait;
    use LabelTrait;
    use AuthorTrait;
    use EventReferenceTrait;

    /**
     * @PiCRUD\Property(
     *      label="Nom d'équipe",
     *      admin={"class": "font-weight-bold"},
     *      form={"class": "order-1"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $label = null;

    /**
     * @PiCRUD\Property(
     *      label="Email de contact",
     *      admin={"class": "font-weight-bold"},
     *      form={"class": "order-2"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255)]
    protected string $email = '';

    #[ORM\ManyToOne(targetEntity: \App\Entity\Event::class, inversedBy: 'teams')]
    protected Event $event;

    /**
     * @PiCRUD\Property(
     *      label="Equipiers",
     *      type="peoples",
     *      form={"class": "order-5"},
     *      options={"entry_type": "App\Form\PeopleFormType"}
     * )
     */
    #[ORM\OneToMany(targetEntity: 'People', mappedBy: 'team', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    protected Collection $peoples;

    #[Pure]
    public function __construct()
    {
        $this->peoples = new ArrayCollection();
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPeoples(): Collection
    {
        return $this->peoples;
    }

    public function setPeoples(Collection $peoples): self
    {
        $this->peoples = $peoples;

        return $this;
    }

    public function addPeople(People $people): self
    {
        $this->peoples->add($people);
        $people->setTeam($this);
        $people->setEvent($this->getEvent());

        return $this;
    }

    public function removePeople(People $people): self
    {
        $this->peoples->remove($people);

        return $this;
    }

    public function contains(Base $base): bool
    {
        if (!empty($base)) {
            return false;
        }

        $this->peoples->rewind();

        while ($this->peoples->valid()) {
            if ($this->peoples->current()->getBase() === $base) {
                return true;
            }

            $this->peoples->next();
        }

        return false;
    }
}

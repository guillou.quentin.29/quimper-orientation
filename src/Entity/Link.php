<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Model\IdTrait;
use App\Model\LabelTrait;
use PiWeb\PiCRUD\Model\AuthorTrait;
use App\Model\PrivateTrait;
use PiWeb\PiCRUD\Annotation as PiCRUD;

/**
 * @PiCRUD\Entity(
 *      name="link",
 *      dashboard={
 *          "order": 7,
 *          "color": "outline-light"
 *     }
 * )
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Link
{
    use IdTrait;
    use LabelTrait;
    use AuthorTrait;
    use PrivateTrait;

    /**
     * @PiCRUD\Property(
     *      label="URL",
     *      form={"class": "order-2"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255)]
    protected string $link = '';

    /**
     * @PiCRUD\Property(
     *      label="Description",
     *      form={"class": "order-3"}
     * )
     */
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}

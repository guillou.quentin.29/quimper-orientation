<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use PiWeb\PiCRUD\Entity\AbstractPiCrudEntity;
use App\Model\PrivateTrait;
use App\Model\ImageTrait;
use App\Model\DocumentTrait;
use App\Model\EventReferenceTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use PiWeb\PiCRUD\Annotation as PiCRUD;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @PiCRUD\Entity(
 *      name="result",
 *      show={},
 *      search=FALSE,
 *      dashboard={
 *          "order": 4,
 *          "format": "extended",
 *          "color": "warning",
 *          "classes": {
 *              "card": "col-12 col-lg-4 col-xl-4"
 *          }
 *     }
 * )
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: \App\Repository\ResultRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Result extends AbstractPiCrudEntity
{
    use PrivateTrait;
    use ImageTrait;
    use DocumentTrait;
    use EventReferenceTrait;

    #[Pure]
    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->image = new EmbeddedFile();
    }

    /**
     * @PiCRUD\Property(
     *      label="Evènement",
     *      admin={},
     *      form={"class": "order-1"}
     * )
     */
    #[ORM\ManyToOne(targetEntity: \App\Entity\Event::class, inversedBy: 'circuits')]
    protected ?Event $event = null;

    /**
     * @PiCRUD\Property(
     *      label="Identifiant course CN",
     *      form={"class": "order-6"}
     * )
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $cn = null;

    /**
     * @PiCRUD\Property(
     *      label="Lien Livelox",
     *      form={"class": "order-6"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $livelox = null;

    public function getCn(): ?int
    {
        return $this->cn;
    }

    public function setCn(?int $cn): self
    {
        $this->cn = $cn;

        return $this;
    }

    public function getLivelox(): ?string
    {
        return $this->livelox;
    }

    public function setLivelox(?string $livelox): self
    {
        $this->livelox = $livelox;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Model\UserNameTrait;
use JetBrains\PhpStorm\Pure;

#[ORM\Entity]
class Base implements \Stringable
{
    use UserNameTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    protected int $id;

    #[ORM\Column(type: 'integer', length: 255, nullable: true)]
    protected ?int $si = null;

    #[ORM\ManyToOne(targetEntity: 'Club', inversedBy: 'members')]
    protected ?Club $club = null;

    #[ORM\Column(type: 'integer', length: 4, nullable: true)]
    protected ?int $categoryId = null;

    #[ORM\Column(type: 'string', length: 4, nullable: true)]
    protected ?string $category = null;

    #[ORM\Column(type: 'integer', length: 4, nullable: true)]
    protected ?int $year = null;

    #[ORM\Column(type: 'string', length: 1, nullable: true)]
    protected ?string $sex = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getSi(): ?int
    {
        return $this->si;
    }

    public function setSi(?int $si): self
    {
        $this->si = $si;

        return $this;
    }

    public function getClub(): Club
    {
        return $this->club;
    }

    public function setClub(Club $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    public function setCategoryId(?int $categoryId): self
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(?string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    #[Pure]
    public function __toString(): string
    {
        return sprintf('%s - %s %s', $this->getId(), $this->getLastName(), $this->getFirstName());
    }
}

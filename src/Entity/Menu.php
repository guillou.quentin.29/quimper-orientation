<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Model\IdTrait;
use App\Model\LabelTrait;
use JetBrains\PhpStorm\Pure;
use PiWeb\PiCRUD\Model\AuthorTrait;
use App\Model\PrivateTrait;
use PiWeb\PiCRUD\Annotation as PiCRUD;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @PiCRUD\Entity(
 *      name="menu",
 *      dashboard={
 *          "order": 6,
 *          "color": "outline-light"
 *     }
 * )
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Menu
{
    use IdTrait;
    use LabelTrait;
    use AuthorTrait;
    use PrivateTrait;

    /**
     * @PiCRUD\Property(
     *      label="URL",
     *      form={"class": "order-2"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255)]
    protected string $link = '';

    /**
     * @ORM\joinColumn(onDelete="SET NULL")
     * @PiCRUD\Property(
     *      label="Sous menu de",
     *      form={"class": "order-3"}
     * )
     */
    #[ORM\ManyToOne(targetEntity: 'Menu', inversedBy: 'childs')]
    protected ?Menu $parent = null;

    #[ORM\OneToMany(targetEntity: 'Menu', mappedBy: 'parent', cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[ORM\OrderBy(['position' => 'ASC'])]
    protected Collection $childs;

    /**
     * @PiCRUD\Property(
     *      label="Position",
     *      form={"class": "order-3"}
     * )
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $position = null;

    #[Pure]
    public function __construct()
    {
        $this->childs = new ArrayCollection();
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getParent(): ?Menu
    {
        return $this->parent;
    }

    public function setParent(?Menu $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChilds(): Collection
    {
        return $this->childs;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Model\UserNameTrait;
use PiWeb\PiCRUD\Annotation as PiCRUD;

/**
 * @PiCRUD\Entity(
 *      name="user",
 *      dashboard={
 *          "order": 5,
 *          "format": "extended",
 *          "color": "danger"
 *     }
 * )
 */
#[UniqueEntity(fields: ['email'], message: 'Un compte existe déjà pour cette adresse e-mail')]
#[ORM\Entity(repositoryClass: \App\Repository\UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface, \Stringable
{
    use UserNameTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private readonly int $id;

    /**
     * @PiCRUD\Property(
     *      label="Email",
     *      admin={"class": "font-weight-bold"},
     * )
     */
    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private string $email = '';

    /**
     * @PiCRUD\Property(
     *      label="Rôles",
     *      type="choices",
     *      form={"choices": {
     *          "Licencié du club": "ROLE_MEMBER",
     *          "Webmaster": "ROLE_WEBMASTER",
     *          "Administrateur": "ROLE_ADMIN"
     *      }}
     * )
     */
    #[ORM\Column(type: 'json')]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column(type: 'string')]
    private string $password = '';

    #[ORM\OneToOne(targetEntity: \App\Entity\Base::class)]
    #[ORM\JoinColumn(name: 'base_id', referencedColumnName: 'id', nullable: true)]
    private ?Base $base = null;
    private ?int $baseId = null;

    /**
     * @var string|null The reset password token or null
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $token = null;

    #[Pure]
    public function __toString(): string
    {
        return sprintf('%s %s', $this->getFirstName(), $this->getLastName()) ?? $this->getUsername();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getBase(): ?Base
    {
        return $this->base;
    }

    public function setBase(?Base $base): self
    {
        $this->base = $base;

        if (!empty($base)) {
            $this->setFirstName($base->getFirstName());
            $this->setLastName($base->getLastName());
        }

        return $this;
    }

    public function getBaseId(): ?int
    {
        return $this->base ? $this->base->getId() : $this->baseId;
    }

    public function setBaseId(?int $id): self
    {
        $this->baseId = $id;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }
}

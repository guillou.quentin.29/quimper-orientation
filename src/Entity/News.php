<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\NewsRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Model\ImageTrait;
use App\Model\DocumentTrait;
use PiWeb\PiCRUD\Entity\AbstractPiCrudEntity;
use App\Model\PrivateTrait;
use App\Model\PromoteTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Doctrine\Common\Collections\ArrayCollection;
use PiWeb\PiCRUD\Annotation as PiCRUD;

/**
 * @PiCRUD\Entity(
 *      name="news",
 *      show={},
 *      dashboard={
 *          "order": 1,
 *          "format": "extended",
 *          "color": "secondary"
 *     }
 * )
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: NewsRepository::class)]
#[ORM\HasLifecycleCallbacks]
class News extends AbstractPiCrudEntity
{
    use ImageTrait;
    use DocumentTrait;
    use PrivateTrait;
    use PromoteTrait;

    public function __construct()
    {
        $this->setPromote(true);
        $this->files = new ArrayCollection();
        $this->image = new EmbeddedFile();
    }
}

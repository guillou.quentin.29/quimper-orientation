<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Model\IdTrait;
use App\Model\LabelTrait;
use PiWeb\PiCRUD\Model\AuthorTrait;
use PiWeb\PiCRUD\Annotation as PiCRUD;

/**
 * @PiCRUD\Entity(
 *      name="format"
 * )
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Format
{
    use IdTrait;
    use LabelTrait;
    use AuthorTrait;
}

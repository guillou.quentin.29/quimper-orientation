<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PeopleRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Model\IdTrait;
use App\Model\UserNameTrait;
use JetBrains\PhpStorm\Pure;
use PiWeb\PiCRUD\Model\AuthorTrait;
use App\Model\EventReferenceTrait;
use PiWeb\PiCRUD\Annotation as PiCRUD;
use App\Validator\UniquePeople;

/**
 * @PiCRUD\Entity(
 *      name="people",
 *      options={"noindex"}
 * )
 * @UniquePeople
 */
#[ORM\Entity(repositoryClass: PeopleRepository::class)]
#[ORM\HasLifecycleCallbacks]
class People implements \Stringable
{
    use IdTrait;
    use UserNameTrait;
    use AuthorTrait;
    use EventReferenceTrait;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $hubId = null;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'peoples')]
    protected Event $event;

    #[ORM\ManyToOne(targetEntity: Base::class)]
    #[ORM\JoinColumn(name: 'base_id', referencedColumnName: 'id', nullable: true)]
    protected ?Base $base = null;
    private ?int $baseId = null;

    #[ORM\ManyToOne(targetEntity: 'Team', cascade: ['persist'], inversedBy: 'peoples')]
    protected ?Team $team = null;

    #[ORM\Column(type: 'string', nullable: true)]
    protected $club;

    /**
     * @PiCRUD\Property(
     *      label="N° puce SportIdent",
     *      form={"class": "order-2"}
     * )
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $si = null;

    /**
     * @PiCRUD\Property(
     *      label="Commentaire",
     *      form={"class": "order-4"}
     * )
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $comment = null;

    /**
     * @PiCRUD\Property(
     *      label="Email de contact",
     *      type="email",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $email = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $position = null;

    /**
     * @PiCRUD\Property(
     *      label="Circuit",
     *      type="circuit",
     *      form={"class": "order-3"}
     * )
     */
    #[ORM\ManyToOne(targetEntity: \App\Entity\Circuit::class, inversedBy: 'peoples')]
    protected ?Circuit $circuit = null;

    public function getHubId(): int|null
    {
        return $this->hubId;
    }

    public function setHubId(int $hubId): People
    {
        $this->hubId = $hubId;

        return $this;
    }

    public function getBase(): ?Base
    {
        return $this->base;
    }

    public function setBase(Base $base): self
    {
        $this->base = $base;
        $this
            ->setFirstName($base->getFirstName())
            ->setLastName($base->getLastName())
            ->setClub($base->getClub());

        return $this;
    }

    public function getBaseId(): ?int
    {
        return $this->base ? $this->base->getId() : $this->baseId;
    }

    public function setBaseId(?int $id): self
    {
        $this->baseId = $id;

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getClub(): ?string
    {
        return $this->club;
    }

    public function setClub($club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getSi(): ?string
    {
        return $this->si;
    }

    public function setSi($si): self
    {
        $this->si = $si;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment($comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getCircuit(): ?Circuit
    {
        return $this->circuit;
    }

    public function setCircuit(?Circuit $circuit): self
    {
        $this->circuit = $circuit;

        return $this;
    }

    #[Pure]
    public function __toString(): string
    {
        return sprintf('%s %s', $this->getFirstName(), $this->getLastName());
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Model\LabelTrait;
use JetBrains\PhpStorm\Pure;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Club implements \Stringable
{
    use LabelTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    protected int $id;

    #[ORM\Column(type: 'string', length: 10)]
    protected ?string $name = null;

    #[ORM\OneToMany(targetEntity: 'Base', cascade: ['persist', 'remove'], mappedBy: 'club')]
    #[ORM\OrderBy(['lastName' => 'ASC', 'firstName' => 'ASC'])]
    protected ?Collection $members = null;

    #[Pure]
    public function __construct()
    {
        $this->members = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMembers(): ?Collection
    {
        return $this->members;
    }

    public function setMembers(Collection $members): self
    {
        $this->members = $members;

        return $this;
    }

    public function addMember(Base $member): self
    {
        $this->members->add($member);

        return $this;
    }

    public function removeMember(Base $member): self
    {
        $this->members->remove($member);

        return $this;
    }

    #[Pure]
    public function __toString(): string
    {
        return sprintf('%s - %s', $this->getName(), $this->getLabel());
    }
}

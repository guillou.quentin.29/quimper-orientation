<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use App\Model\IdTrait;
use App\Model\TitleTrait;
use PiWeb\PiCRUD\Model\AuthorTrait;
use App\Model\PrivateTrait;
use App\Model\ImageTrait;
use PiWeb\PiCRUD\Annotation as PiCRUD;

/**
 * @PiCRUD\Entity(
 *      name="map",
 *      dashboard={
 *          "order": 3,
 *          "format": "extended",
 *          "color": "info"
 *     }
 * )
 * @Vich\Uploadable
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Map
{
    use IdTrait;
    use TitleTrait;
    use AuthorTrait;
    use PrivateTrait;
    use ImageTrait;

    /**
     * @PiCRUD\Property(
     *      label="Déplacement des balises en cours",
     *      type="checkbox",
     *      form={"class": "order-10"}
     * )
     */
    #[ORM\Column(name: 'unavailable', type: 'boolean', nullable: true)]
    protected ?bool $unavailable = false;

    /**
     * @PiCRUD\Property(
     *      label="Fichier",
     *      type="file",
     *      form={"class": "order-3"},
     *      options={"entry_type": "App\Form\DocumentFormType"}
     * )
     */
    #[ORM\ManyToOne(targetEntity: \App\Entity\Document::class, cascade: ['persist'])]
    protected ?Document $file = null;

    /**
     * @PiCRUD\Property(
     *      label="Ville",
     *      form={"class": "order-4"}
     * )
     */
    #[Groups('default')]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $city = '';

    /**
     * @PiCRUD\Property(
     *      label="Latitude",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\Column(type: 'decimal', nullable: true, precision: 8, scale: 6)]
    protected $latitude;

    /**
     * @PiCRUD\Property(
     *      label="Longitude",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\Column(type: 'decimal', nullable: true, precision: 8, scale: 6)]
    protected $longitude;

    #[Pure]
    public function __construct()
    {
        $this->image = new EmbeddedFile();
    }

    public function isUnavailable(): ?bool
    {
        return $this->unavailable;
    }

    public function setUnavailable(?bool $unavailable): self
    {
        $this->unavailable = $unavailable;

        return $this;
    }

    public function getFile(): ?Document
    {
        return $this->file;
    }

    public function setFile(Document $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Model\IdTrait;
use App\Model\LabelTrait;
use JetBrains\PhpStorm\Pure;
use PiWeb\PiCRUD\Model\AuthorTrait;
use App\Model\EventReferenceTrait;
use PiWeb\PiCRUD\Annotation as PiCRUD;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Circuit
{
    use IdTrait;
    use LabelTrait;
    use AuthorTrait;
    use EventReferenceTrait;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $hubId = null;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'circuits')]
    protected Event $event;

    /**
     * @PiCRUD\Property(
     *      label="Distance",
     *      form={"class": "order-2"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $distance = null;

    /**
     * @PiCRUD\Property(
     *      label="Dénivelé",
     *      form={"class": "order-2"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $elevation = null;

    #[ORM\OneToMany(targetEntity: 'People', cascade: ['persist', 'remove'], orphanRemoval: true, mappedBy: 'circuit')]
    protected Collection $peoples;

    public function getHubId(): int|null
    {
        return $this->hubId;
    }

    public function setHubId(int $hubId): Circuit
    {
        $this->hubId = $hubId;

        return $this;
    }

    #[Pure]
    public function __construct()
    {
        $this->peoples = new ArrayCollection();
    }

    public function getPeoples(): Collection
    {
        return $this->peoples;
    }

    public function setDistance(?string $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getDistance(): ?string
    {
        return $this->distance;
    }

    public function setElevation(?string $elevation): self
    {
        $this->elevation = $elevation;

        return $this;
    }

    public function getElevation(): ?string
    {
        return $this->elevation;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use App\Model\IdTrait;
use App\Model\LabelTrait;
use PiWeb\PiCRUD\Model\AuthorTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Document
{
    use IdTrait;
    use LabelTrait;
    use AuthorTrait;

    /**
     * @Vich\UploadableField(mapping="content_file", fileNameProperty="filename")
     *
     * @var File|null
     */
    protected ?File $documentFile = null;

    #[ORM\Column(type: 'string')]
    protected ?string $filename = '';

    public function setDocumentFile(?File $documentFile = null): self
    {
        $this->documentFile = $documentFile;

        return $this;
    }

    public function getDocumentFile(): ?File
    {
        return $this->documentFile;
    }

    public function setFilename(?string $filename): void
    {
        $this->filename = $filename;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }
}

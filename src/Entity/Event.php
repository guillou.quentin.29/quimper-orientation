<?php

declare(strict_types=1);

namespace App\Entity;

use App\Model\EventCancelledTrait;
use App\Model\EventLiveTrait;
use App\Repository\EventRepository;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use PiWeb\PiCRUD\Entity\AbstractPiCrudEntity;
use App\Model\PrivateTrait;
use App\Model\ImageTrait;
use App\Model\DocumentTrait;
use App\Model\EventLocationTrait;
use App\Model\EventEntryTrait;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use PiWeb\PiCRUD\Annotation as PiCRUD;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @PiCRUD\Entity(
 *      name="event",
 *      show={},
 *      search=TRUE,
 *      dashboard={
 *          "order": 2,
 *          "format": "extended",
 *          "color": "primary",
 *          "classes": {
 *              "card": "col-12 col-lg-8 col-xl-8"
 *          }
 *     }
 * )
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: EventRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Event extends AbstractPiCrudEntity
{
    use PrivateTrait;
    use ImageTrait;
    use DocumentTrait;
    use EventLocationTrait;
    use EventEntryTrait;
    use EventCancelledTrait;
    use EventLiveTrait;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $hubId = null;

    /**
     * @PiCRUD\Property(
     *      label="Date de début",
     *      type="datetime",
     *      admin={"options": {"date": "short", "time": "short"}},
     *      form={"class": "order-2"},
     *      search={"class": "order-1", "operator": ">="}
     * )
     */
    #[Groups('default')]
    #[ORM\Column(type: 'datetime')]
    protected DateTime $dateBegin;

    /**
     * @PiCRUD\Property(
     *      label="Date de fin",
     *      type="datetime",
     *      form={"class": "order-2"}
     * )
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected ?DateTime $dateEnd = null;

    /**
     * @PiCRUD\Property(
     *      label="Type d'évènements",
     *      form={"class": "order-3"},
     *      search={"class": "order-2"}
     * )
     */
    #[ORM\ManyToOne(targetEntity: 'Format')]
    protected ?Format $format = null;

    /**
     * @PiCRUD\Property(
     *      label="Organisateur",
     *      form={"class": "order-3"}
     * )
     */
    #[Groups('default')]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $organizer = null;

    /**
     * @PiCRUD\Property(
     *      label="Site web",
     *      form={"class": "order-3"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $website = null;

    /**
     * @PiCRUD\Property(
     *      label="Circuits",
     *      type="circuits",
     *      form={"class": "order-5"},
     *      options={"entry_type": "App\Form\CircuitFormType"}
     * )
     */
    #[ORM\OneToMany(targetEntity: 'Circuit', cascade: ['persist', 'remove'], orphanRemoval: true, mappedBy: 'event')]
    protected Collection $circuits;

    /**
     * @PiCRUD\Property(
     *      label="Evénements associés",
     *      type="events",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\ManyToMany(targetEntity: 'Event', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'event_associated')]
    #[ORM\OrderBy(['dateBegin' => 'ASC'])]
    protected Collection $events;

    public function __construct()
    {
        $this->circuits = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->numberPeopleByEntries = 1;
        $this->image = new EmbeddedFile();
        $this->dateBegin = new DateTime();
        $this->events = new ArrayCollection();
    }

    public function getHubId(): int|null
    {
        return $this->hubId;
    }

    public function setHubId(int $hubId): Event
    {
        $this->hubId = $hubId;

        return $this;
    }

    public function getOrganizer(): ?string
    {
        return $this->organizer;
    }

    public function setOrganizer(?string $organizer): self
    {
        $this->organizer = $organizer;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getDateBegin(): DateTime
    {
        return $this->dateBegin;
    }

    public function setDateBegin(DateTime $dateBegin): self
    {
        $this->dateBegin = $dateBegin;

        if ($this->dateEnd < $dateBegin) {
            $this->dateEnd = null;
        }

        return $this;
    }

    public function getDateEnd(): ?DateTime
    {
        return $this->dateEnd;
    }

    public function setDateEnd($dateEnd): self
    {
        if ($dateEnd < $this->dateBegin) {
            $this->dateEnd = null;
        } else {
            $this->dateEnd = $dateEnd;
        }

        return $this;
    }

    public function getFormat(): ?Format
    {
        return $this->format;
    }

    public function setFormat(Format $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getCircuits(): Collection
    {
        return $this->circuits;
    }

    public function setCircuits(Collection $circuits): self
    {
        $this->circuits = $circuits;

        return $this;
    }

    public function addCircuit($circuit): self
    {
        $circuit->setEvent($this);
        $this->circuits->add($circuit);

        return $this;
    }

    public function removeCircuit($circuit): self
    {
        $this->circuits->remove($circuit);

        return $this;
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function setEvents(Collection $events): self
    {
        $this->events = $events;

        return $this;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
        }

        return $this;
    }

    public function __clone()
    {
        parent::__clone();

        $this->dateBegin = $this->dateBegin->add(new \DateInterval('P7D'));
        if ($this->dateEnd instanceof DateTime) {
            $this->dateEnd = $this->dateEnd->add(new \DateInterval('P7D'));
        }
        if ($this->dateEntries instanceof DateTime) {
            $this->dateEntries = $this->dateEntries->add(new \DateInterval('P7D'));
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Exception;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\GenericEvent;
use App\Form\UsernameFormType;
use App\Form\PasswordFormType;
use App\Entity\User;
use Symfony\Component\Form\FormError;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class SecurityController extends AbstractController
{
    public function __construct(
        private readonly ManagerRegistry $managerRegistry,
    ) {
    }

    #[Route(path: '/logout', name: 'app_logout', priority: 20)]
    public function logout(): never
    {
        throw new LogicException();
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/reset-password', name: 'app_request_reset', priority: 20)]
    public function requestPassword(
        Request $request,
        EventDispatcherInterface $dispatcher
    ): Response {
        if ($this->getUser()) {
             return $this->redirectToRoute('app_profile');
        }

        $form = $this->createForm(UsernameFormType::class, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->managerRegistry
                ->getRepository(User::class)
                ->findOneByEmail($form->get('email')->getData());

            if ($user) {
                $user->setToken(md5(random_bytes(20)));

                $entityManager = $this->managerRegistry->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $event = new GenericEvent($user);
                $dispatcher->dispatch($event, 'security.reset_password');

                return $this->redirectToRoute('app_login');
            }

            $form->addError(new FormError('Cette adresse e-mail n\'existe pas sur notre site.'));
        }

        return $this->render('account/reset_password.html.twig', [
            'usernameForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/reset-password/{token}', name: 'app_reset', priority: 20)]
    public function resetPassword(
        Request $request,
        string $token,
        UserPasswordHasherInterface $passwordHasher
    ): Response {
        $user = $this->managerRegistry
            ->getRepository(User::class)
            ->findOneByToken($token);

        if (!$user) {
            return $this->redirectToRoute('app_homepage');
        }

        $form = $this->createForm(PasswordFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('account/change_password.html.twig', [
            'passwordForm' => $form->createView(),
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\SitemapService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class SitemapController extends AbstractController
{
    public function __construct(
        private readonly SitemapService $sitemapService,
    ) {
    }

    #[Route(path: 'sitemap.xml', name: 'sitemap', defaults: ['_format' => 'xml'], priority: 20)]
    public function index(): Response
    {
        return $this->render('sitemap.xml.twig', [
            'urls' => $this->sitemapService->generate(),
        ]);
    }
}

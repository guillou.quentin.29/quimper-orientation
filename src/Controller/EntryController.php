<?php

declare(strict_types=1);

namespace App\Controller;

use App\Security\EventVoter;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Event;
use App\Entity\Club;
use App\Service\Entry\EntryManager;
use App\Service\Export\ExportManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

final class EntryController extends AbstractController
{
    public function __construct(
        private readonly EntryManager $entryManager,
        private readonly ExportManager $exportManager,
        private readonly EntityManagerInterface $em,
    ) {
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/event/{id}/entry/{mode}/{club?2904}', name: 'app_event_entry', priority: 20)]
    public function entry(Request $request, Event $event, string $mode, ?Club $club): Response
    {
        $this->denyAccessUnlessGranted(EventVoter::REGISTER, $event);

        return $this->entryManager->create($mode)->register($event, $request->get('entry_form'), $this->getUser()) ?
            $this->redirectToRoute('pi_crud_show', [
                'type' => 'event',
                'id' => $event->getid(),
                'slug' => $event->getSlug()
            ]) :
            $this->render('entry/form_' . $mode . '.html.twig', [
                'event' => $event,
                'clubs' => $this->em->getRepository(Club::class)->findAll(),
                'club' => $club
            ]);
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/event/{id}/export/{mode}', name: 'app_event_export', priority: 20)]
    public function export(Event $event, string $mode): Response
    {
        return $this->exportManager->create($mode)->export($event);
    }
}

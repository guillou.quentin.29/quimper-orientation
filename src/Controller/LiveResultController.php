<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\EventRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\SuspiciousOperationException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Event;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

final class LiveResultController extends AbstractController
{
    public function __construct(
        private readonly EventRepository $eventRepository,
        private readonly KernelInterface $kernel
    ) {
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/live', name: 'app_live_list', priority: 20)]
    public function list(): Response
    {
        $filters = [
            'live' => true,
        ];

        if (!$this->isGranted('ROLE_MEMBER')) {
            $filters['private'] = false;
        }

        $events = $this->eventRepository->findBy($filters, ['dateBegin' => 'DESC']);

        return $this->render('live/list.html.twig', [
            'events' => array_map(function (Event $event) {
                $filename = sprintf('%s/public/live/%s.html', $this->kernel->getProjectDir(), $event->getId());

                return [
                    'entity' => $event,
                    'file' => [
                        'updatedAt' => file_exists($filename) ?
                            filemtime($filename) :
                            null,
                    ],
                ];
            }, $events),
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route(path: '/live/event/{event}/upload', name: 'app_live_event_upload', priority: 20)]
    public function upload(Request $request, Event $event): Response
    {
        if ('POST' !== $request->getMethod()) {
            throw new SuspiciousOperationException();
        }

        $file = $request->files->get('file');
        if (!$file instanceof UploadedFile || 'html' !== $file->getClientOriginalExtension()) {
            throw new SuspiciousOperationException();
        }

        try {
            $file->move(
                'live',
                sprintf('%s.html', $event->getId())
            );
        } catch (FileException) {
        }

        return new Response();
    }
}

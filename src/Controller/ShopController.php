<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserFormType;
use App\Entity\Base;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

final class ShopController extends AbstractController
{
    use TargetPathTrait;

    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
        private readonly RequestStack $requestStack,
        private readonly ManagerRegistry $managerRegistry,
    ) {
    }

    #[Route(path: '/shop', name: 'app_shop', priority: 20)]
    public function shop(): Response
    {
        return $this->render('shop/index.html.twig');
    }

    #[Route(path: '/profile/update', name: 'app_profile_update', priority: 20)]
    public function update(Request $request): Response
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app_register');
        }

        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!empty($form->get('baseId')->getData())) {
                $base = $this->managerRegistry
                    ->getRepository(Base::class)
                    ->find($form->get('baseId')->getData());
                $user->setBase($base);
            } else {
                $user->setBase(null);
            }

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('app_profile');
        }

        return $this->render('account/edit.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/profile/delete', name: 'app_profile_delete', priority: 20)]
    public function delete(Request $request): Response
    {
        if ($this->isGranted('ROLE_USER')) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($this->getUser());
            $entityManager->flush();

            $this->tokenStorage->setToken();
            $request->getSession()->invalidate();
        }

        return $this->redirectToRoute('app_homepage');
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\AbstractLoadableEntityRepository;
use App\Repository\ResultRepository;
use App\Service\StructuredDataService;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use PiWeb\PiCRUD\Service\ConfigurationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\NewsRepository;
use App\Repository\EventRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\HttpFoundation\Request;

final class DefaultController extends AbstractController
{
    use TargetPathTrait;

    public const LIMIT_DISPLAYED_NEWS = 6;
    public const LIMIT_DISPLAYED_EVENTS = 4;
    public const LIMIT_DISPLAYED_RESULTS = 3;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly StructuredDataService $structuredDataService,
        private readonly ConfigurationService $configurationService,
        private readonly ManagerRegistry $managerRegistry,
    ) {
    }

    #[Route(path: '/', name: 'app_homepage', priority: 20)]
    public function index(
        Request $request,
        NewsRepository $newsRepository,
        EventRepository $eventRepository,
        ResultRepository $resultRepository,
    ): Response {
        $this->saveTargetPath($this->requestStack->getSession(), 'main', $request->getUri());

        return $this->render('default/homepage.html.twig', [
            'news' => $newsRepository->findByPermission(
                $this->isGranted('ROLE_MEMBER'),
                true,
                self::LIMIT_DISPLAYED_NEWS,
                new DateTime('1 month ago')
            ),
            'events' => $eventRepository->findFiltered(
                !$this->isGranted('ROLE_MEMBER') ?
                    [['name' => 'private', 'operator' => '=', 'value' => false]] :
                    [],
                self::LIMIT_DISPLAYED_EVENTS
            ),
            'results' => $resultRepository->findByPermission(
                $this->isGranted('ROLE_MEMBER'),
                self::LIMIT_DISPLAYED_RESULTS
            ),
            'hasLiveEvents' => $eventRepository->count([
                'live' => true,
            ]),
            'structuredData' => $this->structuredDataService->homepage(),
        ]);
    }

    #[Route(path: '/ajax/load/{type}', name: 'app_ajax_entity_load', priority: 20)]
    public function loadEntity(Request $request, string $type): JsonResponse
    {
        $configuration = $this->configurationService->getEntityConfiguration($type);
        $repository = $this->managerRegistry->getRepository($configuration['class']);
        if (!$repository instanceof AbstractLoadableEntityRepository) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $entities = $repository->loadEntity(
            $this->isGranted('ROLE_MEMBER'),
            intval($request->get('start', 0)),
            self::LIMIT_DISPLAYED_EVENTS
        );

        $html = $this->renderView('default/parts/homepage_mobile_entity_list.html.twig', [
            'entities' => $entities,
            'type' => $type,
        ]);

        return new JsonResponse($html);
    }
}

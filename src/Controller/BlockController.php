<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Link;
use App\Entity\Menu;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class BlockController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    #[Route(path: '/block/link', name: 'app_block_link', priority: 20)]
    public function linkBlockAction(): Response
    {
        return $this->render('partials/block/link.html.twig', [
            'links' => $this->entityManager->getRepository(Link::class)->findBy([]),
        ]);
    }

    #[Route(path: '/block/menu', name: 'app_block_menu', priority: 20)]
    public function menuBlockAction(Request $request): Response
    {
        return $this->render('partials/block/menu.html.twig', [
            'menus' => $this->entityManager->getRepository(Menu::class)->findBy(
                ['parent' => null],
                ['position' => 'ASC']
            ),
            'format' => $request->get('format', 'desktop'),
        ]);
    }
}

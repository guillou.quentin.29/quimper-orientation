<?php

declare(strict_types=1);

namespace App\Validator;

use App\Repository\PeopleRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class UniquePeopleValidator extends ConstraintValidator
{
    public function __construct(
        private readonly PeopleRepository $peopleRepository,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!empty($value->getBase())) {
            $entity = $this->peopleRepository->findOneBy(['event' => $value->getEvent(), 'base' => $value->getBase()]);

            if (!empty($entity) && $value->getId() !== $entity->getId()) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}

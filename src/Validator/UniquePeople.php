<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION", "CLASS"})
 */
class UniquePeople extends Constraint
{
    public string $message = 'Vous êtes déjà inscrit à cet évènement';

    public function getTargets(): array
    {
        return [Constraint::CLASS_CONSTRAINT];
    }
}

<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class AbstractLoadableEntityRepository extends ServiceEntityRepository
{
    abstract function loadEntity(bool $private, int $start, int $limit);
}

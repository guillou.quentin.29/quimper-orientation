<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\People;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

final class PeopleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, People::class);
    }

    public function save(mixed $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(mixed $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByPermission(UserInterface $user): mixed
    {
        return $this->createQueryBuilder('entity')
            ->join('entity.event', 'event')
            ->where('entity.createBy = :user')
            ->orWhere('entity.base = :base')
            ->andWhere('event.dateBegin >= :date')
            ->setParameters([
                'user' => $user->getId(),
                'base' => $user->getBase(),
                'date' => new DateTime('now'),
            ])
            ->orderBy('entity.updateAt', 'DESC')
            ->getQuery()
            ->execute();
    }
}

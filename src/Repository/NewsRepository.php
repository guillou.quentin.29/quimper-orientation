<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\News;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function findByPermission(
        bool $private,
        bool $promote,
        ?int $limit = null,
        ?DateTime $limitDate = null
    ): mixed {
        $query = $this->createQueryBuilder('entity');

        if (!$private) {
            $query->andWhere('entity.private = false');
        }

        if ($promote) {
            $query->andWhere('entity.promote = true');
        }

        if ($limitDate) {
            $query
                ->andWhere('entity.updateAt > :date')
                ->setParameter('date', $limitDate);
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query
            ->orderBy('entity.updateAt', 'DESC')
            ->getQuery()
            ->execute();
    }
}

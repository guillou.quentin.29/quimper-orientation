<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Result;
use Doctrine\Persistence\ManagerRegistry;

final class ResultRepository extends AbstractLoadableEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Result::class);
    }

    public function findByPermission(bool $private, ?int $limit = null): mixed
    {
        $query = $this->createQueryBuilder('entity');

        if (!$private) {
            $query->andWhere('entity.private = false');
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query
            ->orderBy('entity.updateAt', 'DESC')
            ->getQuery()
            ->execute();
    }

    public function loadEntity(bool $private, int $start, int $limit): mixed
    {
        $queryBuilder = $this->createQueryBuilder('e');

        if (!$private) {
            $queryBuilder->andWhere('e.private = false');
        }

        return $queryBuilder
            ->orderBy('e.updateAt', 'DESC')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->execute();
    }
}

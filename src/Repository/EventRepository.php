<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Event;
use App\Entity\People;
use App\Entity\User;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;

final class EventRepository extends AbstractLoadableEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function save(mixed $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(mixed $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findFiltered(array $values = [], $limit = null): mixed
    {
        $query = $this->createQueryBuilder('e');

        foreach ($values as $value) {
            $query
                ->andWhere(sprintf('e.%s %s :%s', $value['name'], $value['operator'], $value['name']))
                ->setParameter($value['name'], $value['value'])
            ;
        }

        $query
            ->andWhere('e.dateBegin > :date')
            ->orWhere('e.dateEnd > e.dateBegin AND e.dateEnd > :date')
            ->setParameter('date', new DateTime())
            ->orderBy('e.dateBegin', 'ASC');

        if (!empty($limit)) {
            $query->setMaxResults($limit);
        }

        return $query
            ->getQuery()
            ->execute();
    }

    public function findByPermission(
        bool $private,
        bool $promote,
        ?int $limit = null,
        ?DateTime $limitDate = null
    ): mixed {
        $query = $this->createQueryBuilder('entity');

        if (!$private) {
            $query->andWhere('entity.private = false');
        }

        if ($promote) {
            $query->andWhere('entity.promote = true');
        }

        if ($limitDate) {
            $query
                ->andWhere('entity.updateAt > :date')
                ->setParameter('date', $limitDate);
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query
            ->orderBy('entity.updateAt', 'DESC')
            ->getQuery()
            ->execute();
    }

    public function loadEntity(bool $private, int $start, int $limit)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        if (!$private) {
            $queryBuilder->andWhere('e.private = false');
        }

        return $queryBuilder
            ->andWhere('e.dateBegin > :date')
            ->orWhere('e.dateEnd > e.dateBegin AND e.dateEnd > :date')
            ->setParameter('date', new DateTime())
            ->orderBy('e.dateBegin', 'ASC')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->execute();
    }

    public function findMyEvents(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        return $queryBuilder
            ->leftJoin(People::class, 'p', 'WITH', 'e.id = p.event')
            ->andWhere('p.base = :base')
            ->andWhere('e.dateBegin > :date')
            ->setParameters([
                'date' => new DateTime(),
                'base' => $user->getBase(),
            ])
            ->orderBy('e.dateBegin', 'ASC')
            ->setMaxResults(3)
            ->getQuery()
            ->execute();
    }
}

<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Circuit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CircuitFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TextType::class, [
                'required' => true,
                'label' => 'Libellé',
                'attr' => [
                    'class' => 'form-control pr-4'
                ],
                'label_attr' => [
                    'class' => 'pr-2'
                ]
            ])
            ->add('distance', TextType::class, [
                'required' => false,
                'label' => 'Distance',
                'attr' => [
                    'class' => 'form-control pr-4'
                ],
                'label_attr' => [
                    'class' => 'pr-2'
                ]
            ])
            ->add('elevation', TextType::class, [
                'required' => false,
                'label' => 'Dénivelé',
                'attr' => [
                    'class' => 'form-control pr-4'
                ],
                'label_attr' => [
                    'class' => 'pr-2'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Circuit::class,
        ]);
    }
}

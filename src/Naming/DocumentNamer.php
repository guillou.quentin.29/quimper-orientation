<?php

declare(strict_types=1);

namespace App\Naming;

use App\Entity\Document;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Naming\SmartUniqueNamer;

final class DocumentNamer implements NamerInterface
{
    public function __construct(
        private readonly SluggerInterface $slugger,
        private readonly SmartUniqueNamer $smartUniqueNamer,
    ) {
    }

    public function name($object, PropertyMapping $mapping): string
    {
        if (!$object instanceof Document) {
            return $this->smartUniqueNamer->name($object, $mapping);
        }

        $file = $object->getDocumentFile();
        if (!$file instanceof UploadedFile) {
            return $this->smartUniqueNamer->name($object, $mapping);
        }

        $extension = $file->getClientOriginalExtension();

        return sprintf(
            '%s-%s.%s',
            $this->slugger->slug($object->getLabel())->lower(),
            uniqid('', true),
            $extension
        );
    }
}

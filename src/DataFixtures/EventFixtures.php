<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\Format;
use DateInterval;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    private const TITLE_ORIENTEERING_SCHOOL = 'Ecole de CO n°%s';
    private const TITLE_TRAINING = 'Entrain. Pole Espoir';
    private const TITLE_MEETING = 'AG du club';
    private const ORGANIZER = 'L\'équipe du Quimper Orientation';

    public function load(ObjectManager $manager): void
    {
        /** @var Format $orienteeringSchoolFormat */
        $orienteeringSchoolFormat = $this->getReference('format_orienteering_school');
        /** @var Format $middleFormat */
        $middleFormat = $this->getReference('format_middle');

        $dateBegin = new DateTime('-1 month');

        $pastEvent = new Event();
        $pastEvent
            ->setTitle(sprintf(self::TITLE_ORIENTEERING_SCHOOL, 1))
            ->setDateBegin($dateBegin)
            ->setLocationTitle('Quimper')
            ->setFormat($orienteeringSchoolFormat)
        ;
        $manager->persist($pastEvent);
        $this->addReference('event_orienteering_school_1', $pastEvent);

        $dateBegin = new DateTime('+1 day');
        $dateEntries = (clone $dateBegin)->sub(new DateInterval('P2D'));

        $nextEvent = new Event();
        $nextEvent
            ->setTitle(self::TITLE_TRAINING)
            ->setDateBegin($dateBegin)
            ->setDateEntries($dateEntries)
            ->setLocationTitle('Quimper')
            ->setOrganizer(self::ORGANIZER)
        ;
        $manager->persist($nextEvent);
        $this->addReference('event_training_1', $nextEvent);

        $privateEvent = new Event();
        $privateEvent
            ->setTitle(self::TITLE_MEETING)
            ->setDateBegin($dateBegin)
            ->setDateEntries($dateBegin)
            ->setLocationTitle('Rosporden')
            ->setOrganizer(self::ORGANIZER)
            ->setFormat($middleFormat)
            ->setPrivate(true)
        ;
        $manager->persist($privateEvent);
        $this->addReference('event_middle_1', $privateEvent);

        $dateBegin = new DateTime('+1 week');
        $dateEnd = (clone $dateBegin)->add(new DateInterval('P1D'));
        $dateEntries = (clone $dateBegin)->sub(new DateInterval('P2D'));

        $multiDaysEvent = new Event();
        $multiDaysEvent
            ->setTitle(sprintf(self::TITLE_ORIENTEERING_SCHOOL, 2))
            ->setDateBegin($dateBegin)
            ->setDateEnd($dateEnd)
            ->setDateEntries($dateEntries)
            ->setFormat($orienteeringSchoolFormat)
            ->setOrganizer(self::ORGANIZER)
        ;
        $manager->persist($multiDaysEvent);
        $this->addReference('event_orienteering_school_2', $multiDaysEvent);

        $dateBegin = new DateTime('+2 week');
        $dateEnd = (clone $dateBegin)->add(new DateInterval('P2M5D'));
        $dateEntries = (clone $dateBegin)->sub(new DateInterval('P6D'));

        $multiMonthsEvent = new Event();
        $multiMonthsEvent
            ->setTitle(sprintf(self::TITLE_ORIENTEERING_SCHOOL, 3))
            ->setDateBegin($dateBegin)
            ->setDateEnd($dateEnd)
            ->setDateEntries($dateEntries)
            ->setLocationTitle('Rennes')
            ->setFormat($orienteeringSchoolFormat)
        ;
        $manager->persist($multiMonthsEvent);
        $this->addReference('event_orienteering_school_3', $multiMonthsEvent);

        $dateBegin = new DateTime('+3 week');

        $noEntriesEvent = new Event();
        $noEntriesEvent
            ->setTitle(sprintf(self::TITLE_ORIENTEERING_SCHOOL, 4))
            ->setDateBegin($dateBegin)
            ->setAllowEntries(false)
            ->setLocationTitle('Concarneau')
            ->setFormat($orienteeringSchoolFormat)
        ;
        $manager->persist($noEntriesEvent);
        $this->addReference('event_orienteering_school_4', $noEntriesEvent);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            FormatFixtures::class,
        ];
    }
}

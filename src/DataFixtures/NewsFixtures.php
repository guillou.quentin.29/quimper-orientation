<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\News;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class NewsFixtures extends Fixture
{
    public function __construct(
        private readonly string $rootDir,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $htmlContent = file_get_contents(sprintf('%s/src/DataFixtures/content/news.txt', $this->rootDir));

        $mainArticle = new News();
        $mainArticle
            ->setTitle('Retours du weekend du CFC 2022 en Corrèze')
            ->setContent($htmlContent)
        ;
        $manager->persist($mainArticle);

        $notPromoteArticle = new News();
        $notPromoteArticle
            ->setTitle('Le club')
            ->setContent('Présentation du club')
            ->setPromote(false)
        ;
        $manager->persist($notPromoteArticle);

        $manager->flush();
    }
}

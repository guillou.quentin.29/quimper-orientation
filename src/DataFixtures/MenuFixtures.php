<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Menu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MenuFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $club = new Menu();
        $club
            ->setLabel('Le club')
            ->setPosition(0)
        ;
        $manager->persist($club);

        $presentation = new Menu();
        $presentation
            ->setLabel('Présentation')
            ->setLink('/')
            ->setParent($club)
            ->setPosition(1)
        ;
        $manager->persist($presentation);

        $joinUs = new Menu();
        $joinUs
            ->setLabel('Nous rejoindre')
            ->setLink('/')
            ->setParent($club)
            ->setPosition(2)
        ;
        $manager->persist($joinUs);

        $contact = new Menu();
        $contact
            ->setLabel('Contact')
            ->setLink('/')
            ->setParent($club)
            ->setPosition(3)
        ;
        $manager->persist($contact);

        $news = new Menu();
        $news
            ->setLabel('Actualités')
            ->setLink('/news')
            ->setPosition(1)
        ;
        $manager->persist($news);

        $event = new Menu();
        $event
            ->setLabel('Événement')
            ->setLink('/event')
            ->setPosition(2)
        ;
        $manager->persist($event);

        $result = new Menu();
        $result
            ->setLabel('Résultats')
            ->setLink('/result')
            ->setPosition(3)
        ;
        $manager->persist($result);

        $map = new Menu();
        $map
            ->setLabel('Parcours permanents')
            ->setLink('/map')
            ->setPosition(4)
        ;
        $manager->persist($map);

        $manager->flush();
    }
}

<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Map;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MapFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $quimper1 = new Map();
        $quimper1
            ->setTitle('Bois de Keradennec')
            ->setCity('Quimper')
            ->setLatitude(47.9776849021983)
            ->setLongitude(-4.085953044477343)
        ;
        $manager->persist($quimper1);

        $quimper2 = new Map();
        $quimper2
            ->setTitle('Bois d\'Amour')
            ->setCity('Quimper')
            ->setLatitude(47.996917445138976)
            ->setLongitude(-4.130289912886797)
        ;
        $manager->persist($quimper2);

        $quimper3 = new Map();
        $quimper3
            ->setTitle('Bois du Séminaire')
            ->setCity('Quimper')
            ->setLatitude(47.99012473242732)
            ->setLongitude(-4.121862725543377)
        ;
        $manager->persist($quimper3);

        $bannalec1 = new Map();
        $bannalec1
            ->setTitle('Stade Jean Bourhis')
            ->setCity('Bannalec')
            ->setLatitude(47.928696598131246)
            ->setLongitude(-3.6878011018689136)
        ;
        $manager->persist($bannalec1);

        $manager->flush();
    }
}

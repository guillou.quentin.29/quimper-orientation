<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Link;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LinkFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $ffco = new Link();
        $ffco
            ->setLabel('Fédération Française de Course d\'Orientation')
            ->setLink('https://www.ffcorientation.fr/')
        ;
        $manager->persist($ffco);

        $manager->flush();
    }
}

<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Format;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FormatFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $middle = new Format();
        $middle
            ->setLabel('Moyenne distance')
        ;
        $manager->persist($middle);
        $this->addReference('format_middle', $middle);

        $long = new Format();
        $long
            ->setLabel('Longue distance')
        ;
        $manager->persist($long);
        $this->addReference('format_long', $long);

        $orienteeringSchool = new Format();
        $orienteeringSchool
            ->setLabel('Ecole de CO')
        ;
        $manager->persist($orienteeringSchool);
        $this->addReference('format_orienteering_school', $orienteeringSchool);

        $manager->flush();
    }
}

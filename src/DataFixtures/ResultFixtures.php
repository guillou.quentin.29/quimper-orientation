<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\Result;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ResultFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /** @var Event $event */
        $event = $this->getReference('event_orienteering_school_1');

        $result = new Result();
        $result
            ->setTitle('Ecole de CO n°1')
            ->setEvent($event)
            ->setContent('Les résultats des différents exercices qui ont été proposés lors de cette première école de CO')
            ->setCn(12)
        ;
        $manager->persist($result);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            EventFixtures::class,
        ];
    }
}

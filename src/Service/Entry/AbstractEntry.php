<?php

declare(strict_types=1);

namespace App\Service\Entry;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Validator\UniquePeople;

abstract class AbstractEntry implements EntryInterface
{
    public function __construct(
        protected EntityManagerInterface $em,
        private readonly ValidatorInterface $validator,
        private readonly RequestStack $requestStack,
    ) {
    }

    protected function save(object $entity): void
    {
        $violations = $this->validator->validate($entity, [new UniquePeople()]);

        if ($violations->count() === 0) {
            $this->em->persist($entity);
            $this->requestStack->getSession()->getFlashBag()->add('success', 'Inscription de ' . $entity . ' réussie');
        } else {
            $this->requestStack->getSession()->getFlashBag()->add('warning', $entity . ' est déjà inscrit');
        }
    }
}

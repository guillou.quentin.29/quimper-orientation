<?php

declare(strict_types=1);

namespace App\Service\Entry;

use App\Entity\Event;
use Symfony\Component\Security\Core\User\UserInterface;

interface EntryInterface
{
    function register(Event $event, ?array $entry, ?UserInterface $user): bool;
}

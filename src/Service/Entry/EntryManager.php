<?php

declare(strict_types=1);

namespace App\Service\Entry;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class EntryManager
{
    public function __construct(
        private readonly EntryDiscovery $discovery,
        private readonly EntityManagerInterface $em,
        private readonly ValidatorInterface $validator,
        private readonly RequestStack $requestStack,
    ) {
    }

    public function getEntries(): array
    {
        return $this->discovery->getEntries();
    }

    /**
     * @throws Exception
     */
    public function getEntry($name): array
    {
        $entries = $this->discovery->getEntries();
        if (isset($entries[$name])) {
            return $entries[$name];
        }

        throw new Exception('Entry not found.');
    }

    /**
     * @throws Exception
     */
    public function create($name): EntryInterface
    {
        $entries = $this->discovery->getEntries();
        if (array_key_exists($name, $entries)) {
            $class = $entries[$name]['class'];
            if (!class_exists($class)) {
                throw new Exception('Entry class does not exist.');
            }
            return new $class($this->em, $this->validator, $this->requestStack);
        }

        throw new Exception('Entry does not exist.');
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Entry;

use App\Annotation\Entry;
use Doctrine\Common\Annotations\Reader;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

final class EntryDiscovery
{
    private readonly string $namespace;
    private readonly string $directory;
    private array $entries = [];

    public function __construct(
        private readonly string $rootDir,
        private readonly Reader $annotationReader,
    ) {
        $this->namespace = 'App\Service\Entry';
        $this->directory = 'Service/Entry';
    }

    public function getEntries(): array
    {
        if (!$this->entries) {
            $this->discoverEntries();
        }

        return $this->entries;
    }

    private function discoverEntries(): void
    {
        $path = $this->rootDir . '/src/' . $this->directory;
        $finder = new Finder();
        $finder->files()->in($path);

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $class = $this->namespace . '\\' . $file->getBasename('.php');
            try {
                $annotation = $this->annotationReader->getClassAnnotation(new ReflectionClass($class), Entry::class);
                if (!$annotation) {
                    continue;
                }
            } catch (ReflectionException) {
                continue;
            }

            /** @var Entry $annotation */
            $this->entries[$annotation->getName()] = [
                'class' => $class,
                'annotation' => $annotation,
            ];
        }
    }
}

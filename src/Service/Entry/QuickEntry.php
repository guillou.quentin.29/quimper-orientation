<?php

declare(strict_types=1);

namespace App\Service\Entry;

use App\Annotation\Entry;
use App\Entity\Event;
use App\Entity\Circuit;
use App\Entity\People;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Entry(
 *     name="quick"
 * )
 */
final class QuickEntry extends AbstractEntry
{
    public function register(Event $event, ?array $entry = [], ?UserInterface $user = null): bool
    {
        if ((count($event->getCircuits()) > 0 && empty($entry)) || empty($user)) {
            return false;
        }

        $circuitRepository = $this->em->getRepository(Circuit::class);
        $circuit = !empty($entry['circuit']) ? $circuitRepository->findOneById($entry['circuit']) : null;

        $people = new People();

        if (!empty($user->getBase())) {
            $people->setBase($user->getBase());
        }

        $people
            ->setEvent($event)
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setCircuit($circuit)
            ->setComment($entry['comment'] ?? null)
            ->setCreateBy($user)
            ->setUpdateBy($user)
        ;

        if (!empty($user->getBase())) {
            $people
                ->setClub($user->getBase()->getClub())
                ->setSi((string)$user->getBase()->getSi())
            ;
        }

        $this->save($people);
        $this->em->flush();

        return true;
    }
}

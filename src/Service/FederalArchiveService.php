<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Base;
use App\Entity\Club;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

final class FederalArchiveService
{
    public const COLUMN_SI_NUMBER = 1;
    public const COLUMN_BASE_ID = 3;
    public const COLUMN_LAST_NAME = 4;
    public const COLUMN_FIRST_NAME = 5;
    public const COLUMN_YEAR = 6;
    public const COLUMN_SEX = 7;
    public const COLUMN_CLUB = 8;
    public const COLUMN_CLUB_NAME = 9;
    public const COLUMN_CLUB_VILLE = 10;
    public const COLUMN_CATEGORY_ID = 14;
    public const COLUMN_CATEGORY = 15;

    private $file;

    public function __construct(
        private readonly EntityManagerInterface $em,
    ) {
    }

    public function setFile($file): self
    {
        $this->file = $file;

        return $this;
    }

    public function update(): bool
    {
        if (empty($this->file)) {
            throw new BadRequestException();
        }

        if ('0' === date('z')) {
            $this->em->createQuery('UPDATE App\Entity\Base b SET b.club = 9999')->getResult();
        }

        fgetcsv($this->file, 0, ";");
        while (($row = fgetcsv($this->file, 0, ";")) !== false) {
            if (!empty($row[self::COLUMN_BASE_ID]) && !empty($row[self::COLUMN_CLUB])) {
                $club = $this->saveClub($row);
                $this->saveBase($club, $row);
            }
        }

        $this->em->flush();

        return true;
    }

    private function saveBase(Club $club, array $row): void
    {
        $base = $this->em->getRepository(Base::class)->find($row[self::COLUMN_BASE_ID]);

        if (empty($base)) {
            $base = new Base();
            $base->setId((int) $row[self::COLUMN_BASE_ID]);
        }

        $base
          ->setFirstName(utf8_encode((string) $row[self::COLUMN_FIRST_NAME]))
          ->setLastName(utf8_encode((string) $row[self::COLUMN_LAST_NAME]))
          ->setSI(intval($row[self::COLUMN_SI_NUMBER]))
          ->setClub($club)
          ->setCategoryId(intval($row[self::COLUMN_CATEGORY_ID]))
          ->setCategory($row[self::COLUMN_CATEGORY])
          ->setYear(intval($row[self::COLUMN_YEAR]))
          ->setSex($row[self::COLUMN_SEX])
        ;

        $this->em->persist($base);
    }

    private function saveClub(array $row): Club
    {
        $club = $this->em->getRepository(Club::class)->find($row[self::COLUMN_CLUB]);

        if (empty($club)) {
            $club = new Club();
            $club->setId((int) $row[self::COLUMN_CLUB]);
        }

        $club
          ->setName(utf8_encode((string) $row[self::COLUMN_CLUB_NAME]))
          ->setLabel(utf8_encode((string) $row[self::COLUMN_CLUB_VILLE]))
        ;

        $this->em->persist($club);
        $this->em->flush();

        return $club;
    }
}

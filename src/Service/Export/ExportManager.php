<?php

declare(strict_types=1);

namespace App\Service\Export;

use Exception;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

final class ExportManager
{
    public function __construct(
        private readonly ExportDiscovery $discovery,
        private readonly Environment $twig,
        private readonly Pdf $pdf,
        private readonly RouterInterface $router,
        private readonly RequestStack $requestStack,
    ) {
    }

    public function getExports(): array
    {
        return $this->discovery->getExports();
    }

    /**
     * @throws Exception
     */
    public function getExport($name): array
    {
        $exports = $this->discovery->getExports();
        if (isset($exports[$name])) {
            return $exports[$name];
        }

        throw new Exception('Export not found.');
    }

    /**
     * @throws Exception
     */
    public function create($name): ExportInterface
    {
        $exports = $this->discovery->getExports();
        if (array_key_exists($name, $exports)) {
            $class = $exports[$name]['class'];
            if (!class_exists($class)) {
                throw new Exception('Export class does not exist.');
            }
            return new $class(
                $this->twig,
                $this->pdf,
                $this->router,
                $this->requestStack->getCurrentRequest()?->getSession(),
            );
        }

        throw new Exception('Export does not exist.');
    }
}

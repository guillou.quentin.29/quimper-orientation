<?php

declare(strict_types=1);

namespace App\Service\Export;

use App\Entity\Event;
use Symfony\Component\HttpFoundation\Response;

interface ExportInterface
{
    function export(Event $event): Response;
}

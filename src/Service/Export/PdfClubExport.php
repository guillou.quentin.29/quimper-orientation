<?php

declare(strict_types=1);

namespace App\Service\Export;

use App\Annotation\Export;
use App\Entity\Event;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Export(
 *     name="pdf-club"
 * )
 */
final class PdfClubExport implements ExportInterface
{
    public function __construct(
        private readonly Environment $twig,
        private readonly Pdf $pdf,
        private readonly RouterInterface $router,
        private readonly SessionInterface $session,
    ) {
    }

    public function export(Event $event): Response
    {
        try {
            if ($event->getNumberPeopleByEntries() > 1) {
                $html = $this->twig->render('entry/export_pdf_club_teams.html.twig', [
                    'event'  => $event
                ]);
            } else {
                $html = $this->twig->render('entry/export_pdf_club_individuals.html.twig', [
                    'event'  => $event
                ]);
            }
        } catch (LoaderError | RuntimeError | SyntaxError) {
            $this->session->getFlashBag()->add('error', 'Une erreur est survenue lors de la génération du PDF');

            return new RedirectResponse($this->router->generate('pi_crud_show', [
                'type' => 'event',
                'id' => $event->getid(),
                'slug' => $event->getSlug(),
            ]));
        }

        return new PdfResponse($this->pdf->getOutputFromHtml($html), 'file.pdf');
    }
}

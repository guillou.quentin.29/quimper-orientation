<?php

declare(strict_types=1);

namespace App\Service\Export;

use App\Annotation\Export;
use App\Entity\Event;
use App\Entity\People;
use Sonata\Exporter\Handler;
use Sonata\Exporter\Writer\CsvWriter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Sonata\Exporter\Source\ArraySourceIterator;

/**
 * @Export(
 *     name="csv"
 * )
 */
final class CsvExport implements ExportInterface
{
    public function export(Event $event): Response
    {
        $writer = new CsvWriter('php://output', ';', '"', '\\', false, true);
        $contentType = 'text/csv';

        $source = new ArraySourceIterator($this->data($event));

        $callback = static function () use ($source, $writer) {
            $handler = Handler::create($source, $writer);
            $handler->export();
        };

        return new StreamedResponse($callback, 200, [
            'Content-Type' => $contentType,
            'Content-Disposition' => sprintf(
                'attachment; filename="%s"',
                sprintf('%s.csv', $event->getSlug() ?? 'export')
            ),
        ]);
    }

    private function data(Event $event): array
    {
        $data = [];
        $data[] = $this->header();

        /** @var People $people */
        foreach ($event->getPeoples() as $people) {
            $data[] = [
                $people->getFirstName(),
                $people->getLastName(),
                $people->getCircuit()?->getId(),
                $people->getCircuit()?->getLabel(),
                $people->getComment(),
                $people->getEmail(),
            ];
        }

        return $data;
    }

    private function header(): array
    {
        return [
            'firstname',
            'lastname',
            'circuit_id',
            'circuit_name',
            'comment',
            'email'
        ];
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Export;

use App\Annotation\Export;
use App\Entity\Event;
use App\Entity\People;
use App\Entity\Team;
use Sonata\Exporter\Handler;
use Sonata\Exporter\Writer\CsvWriter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Sonata\Exporter\Source\ArraySourceIterator;

/**
 * @Export(
 *     name="geraid"
 * )
 */
final class GeRaidExport implements ExportInterface
{
    public function export(Event $event): Response
    {
        $writer = new CsvWriter('php://output', ';', '"', '\\', false);
        $contentType = 'text/csv; charset=UTF-8';

        $source = new ArraySourceIterator($this->data($event));

        $callback = static function () use ($source, $writer) {
            $handler = Handler::create($source, $writer);
            $handler->export();
        };

        return new StreamedResponse($callback, 200, [
            'Content-Encoding' => 'UTF-8',
            'Content-Type' => $contentType,
            'Content-Disposition' => sprintf('attachment; filename="%s"', 'export_geraid.csv'),
        ]);
    }

    private function data(Event $event): array
    {
        $data = [];
        $data[] = $this->header();

        /** @var Team $team */
        foreach ($event->getTeams() as $team) {
            $line = [
                $team->getId(),
                $team->getLabel(),
                "",
                "",
            ];

            /** @var People $people */
            foreach ($team->getPeoples() as $people) {
                array_push($line, $people->getLastName(), $people->getFirstName());
            }

            $data[] = $line;
        }

        return $data;
    }

    private function header(): array
    {
        return [
            "Dossard",
            "Nom",
            "Puce",
            "Categorie",
        ];
    }
}

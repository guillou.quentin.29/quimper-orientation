<?php

declare(strict_types=1);

namespace App\Service\Export;

use App\Annotation\Export;
use App\Entity\Event;
use Sonata\Exporter\Handler;
use Sonata\Exporter\Writer\CsvWriter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Sonata\Exporter\Source\ArraySourceIterator;

/**
 * @Export(
 *     name="oe2010"
 * )
 */
final class Oe2010Export implements ExportInterface
{
    public function export(Event $event): Response
    {
        $writer = new CsvWriter('php://output', ';', '"', '\\', false);
        $contentType = 'text/csv; charset=UTF-8';

        $source = new ArraySourceIterator($this->data($event));

        $callback = static function () use ($source, $writer) {
            $handler = Handler::create($source, $writer);
            $handler->export();
        };

        return new StreamedResponse($callback, 200, [
            'Content-Encoding' => 'UTF-8',
            'Content-Type' => $contentType,
            'Content-Disposition' => sprintf('attachment; filename="%s"', 'export_oe2010.csv'),
        ]);
    }

    private function data(Event $event): array
    {
        $data = [];
        $data[] = $this->header();

        foreach ($event->getPeoples() as $people) {
            $categoryId = !empty($people->getCircuit()) ?
                $people->getCircuit()->getId() :
                (!empty($people->getBase()) ? $people->getBase()->getCategoryId() : "");

            $category = !empty($people->getCircuit()) ?
                $people->getCircuit()->getLabel() :
                (!empty($people->getBase()) ? $people->getBase()->getCategory() : "");

            $data[] = [
                "",
                "",
                "",
                $people->getSi(), // Puce
                !(empty($people->getBase())) ? $people->getBase()->getId() : "",
                $people->getLastName(), // Nom
                $people->getFirstName(), // Prénom
                !(empty($people->getBase())) ? $people->getBase()->getYear() : "", // Né
                !(empty($people->getBase())) ? $people->getBase()->getSex() : "", // Sexe
                0,
                0,
                "",
                "",
                "",
                0,
                "",
                "",
                "",
                !(empty($people->getBase())) ?
                    $people->getBase()->getClub()->getId() :
                    "10000", // Numéro club
                !(empty($people->getBase())) ?
                    $people->getBase()->getClub()->getName() :
                    "10000PO", // Nom club
                !(empty($people->getBase())) ?
                    $people->getBase()->getClub()->getLabel() :
                    "Pass-Orientation", // Nom complet
                "FR", // Nationalité
                "",
                "",
                $categoryId, // Numéro de catégorie
                $category, // Libellé de catégorie
                $category, // Libellé de catégorie
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "", // Nr Circuit
                "", // Circuit
                "",
                "",
                "",
            ];
        }

        return $data;
    }

    private function header(): array
    {
        return [
            "OE0001",
            "Doss.",
            "X Nr",
            "Nr puce",
            "Ident. base de données",
            "NOM",
            "Prénom",
            "Né",
            "S",
            "Plage",
            "nc",
            "Départ",
            "Arrivée",
            "Temps",
            "Classer",
            "Bonification -",
            "Pénalité +",
            "Commentaire",
            "Nr club",
            "Nom club",
            "Ville",
            "Nat",
            "Groupe",
            "Région",
            "Nr catg.",
            "Catg. Court",
            "Catg. Long",
            "Nr Catg. Inscription",
            "Catg. d'inscription (court)",
            "Catg. d'inscription (long)",
            "Ranking",
            "Points Ranking",
            "Num1",
            "Num2",
            "Num3",
            "Text1",
            "Text2",
            "Text3",
            "Adr. nom de famille",
            "Adr. prénom",
            "Rue",
            "Ligne2",
            "CP",
            "Adr. ville",
            "Tél.",
            "Mobile",
            "Fax",
            "Email",
            "Louée",
            "Tarif inscription",
            "Payé",
            "Equipe",
            "Nr circuit",
            "Circuit",
            "km",
            "m",
            "Postes du circuit",
        ];
    }
}

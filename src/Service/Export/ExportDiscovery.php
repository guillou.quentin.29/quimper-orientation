<?php

declare(strict_types=1);

namespace App\Service\Export;

use App\Annotation\Export;
use Doctrine\Common\Annotations\Reader;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

final class ExportDiscovery
{
    private readonly string $namespace;
    private readonly string $directory;
    private array $exports = [];

    public function __construct(
        private $rootDir,
        private readonly Reader $annotationReader,
    ) {
        $this->namespace = 'App\Service\Export';
        $this->directory = 'Service/Export';
    }

    public function getExports(): array
    {
        if (!$this->exports) {
            $this->discoverExports();
        }

        return $this->exports;
    }

    private function discoverExports(): void
    {
        $path = $this->rootDir . '/src/' . $this->directory;
        $finder = new Finder();
        $finder->files()->in($path);

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $class = $this->namespace . '\\' . $file->getBasename('.php');
            try {
                $annotation = $this->annotationReader->getClassAnnotation(new ReflectionClass($class), Export::class);
                if (!$annotation) {
                    continue;
                }
            } catch (ReflectionException) {
                continue;
            }

            /** @var Export $annotation */
            $this->exports[$annotation->getName()] = [
                'class' => $class,
                'annotation' => $annotation,
            ];
        }
    }
}

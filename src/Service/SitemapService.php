<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Map;
use App\Entity\Result;
use App\Repository\EventRepository;
use App\Repository\NewsRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

final class SitemapService
{
    public function __construct(
        private readonly RouterInterface $router,
        private readonly NewsRepository $newsRepository,
        private readonly EventRepository $eventRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function generate(): array
    {
        return array_merge(
            $this->generateDefaultsUrls(),
            $this->generateNewsUrls(),
            $this->generateEventsUrls(),
            $this->generateResultsUrls(),
            $this->generateMapsUrls(),
        );
    }

    private function generateDefaultsUrls(): array
    {
        return [
            $this->format(
                $this->router->generate('app_homepage', [], UrlGeneratorInterface::ABSOLUTE_URL),
                new DateTime(),
            ),
        ];
    }

    private function generateNewsUrls(): array
    {
        $urls = [];

        $listUpdatedAt = null;

        $news = $this->newsRepository->findByPermission(false, false);
        foreach ($news as $new) {
            $urls[] = $this->format(
                $this->router->generate(
                    'pi_crud_show',
                    ['type' => 'news', 'id' => $new->getId(), 'slug' => $new->getSlug()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                $new->getUpdateAt(),
                'monthly',
                1
            );

            $listUpdatedAt = empty($listUpdatedAt) || $new->getUpdateAt() > $listUpdatedAt ?
                $new->getUpdateAt() :
                $listUpdatedAt;
        }

        $urls[] = $this->format(
            $this->router->generate(
                'pi_crud_list',
                ['type' => 'news'],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $listUpdatedAt,
            'monthly',
            0.2
        );

        return $urls;
    }

    private function generateEventsUrls(): array
    {
        $urls = [];

        $listUpdatedAt = null;

        $events = $this->eventRepository->findFiltered([
            'private' => ['name' => 'private', 'operator' => '=', 'value' => false]
        ]);
        foreach ($events as $event) {
            $urls[] = $this->format(
                $this->router->generate(
                    'pi_crud_show',
                    ['type' => 'event', 'id' => $event->getId(), 'slug' => $event->getSlug()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                $event->getUpdateAt(),
                'monthly',
                1
            );

            $listUpdatedAt = empty($listUpdatedAt) || $event->getUpdateAt() > $listUpdatedAt ?
                $event->getUpdateAt() :
                $listUpdatedAt;
        }

        $urls[] = $this->format(
            $this->router->generate(
                'pi_crud_list',
                ['type' => 'event'],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $listUpdatedAt,
            'monthly',
        );

        return $urls;
    }

    private function generateResultsUrls(): array
    {
        $urls = [];

        $listUpdatedAt = null;

        $results = $this->entityManager->getRepository(Result::class)->findAll();
        foreach ($results as $result) {
            $urls[] = $this->format(
                $this->router->generate(
                    'pi_crud_show',
                    ['type' => 'result', 'id' => $result->getId(), 'slug' => $result->getSlug()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                $result->getUpdateAt(),
                'monthly',
                1
            );

            $listUpdatedAt = empty($listUpdatedAt) || $result->getUpdateAt() > $listUpdatedAt ?
                $result->getUpdateAt() :
                $listUpdatedAt;
        }

        $urls[] = $this->format(
            $this->router->generate(
                'pi_crud_list',
                ['type' => 'result'],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $listUpdatedAt,
            'monthly',
        );

        return $urls;
    }

    private function generateMapsUrls(): array
    {
        $urls = [];

        $listUpdatedAt = null;

        $maps = $this->entityManager->getRepository(Map::class)->findAll();
        foreach ($maps as $map) {
            $listUpdatedAt = empty($listUpdatedAt) || $map->getUpdateAt() > $listUpdatedAt ?
                $map->getUpdateAt() :
                $listUpdatedAt;
        }

        $urls[] = $this->format(
            $this->router->generate(
                'pi_crud_list',
                ['type' => 'map'],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $listUpdatedAt,
            'monthly',
        );

        return $urls;
    }

    #[ArrayShape([
        'loc' => "string",
        'lastmod' => "null|string",
        'frequency' => "string",
        'priority' => "float",
    ])]
    private function format(string $url, ?DateTime $date, string $frequency = 'daily', float $priority = 0.5): array
    {
        return [
            'loc' => $url,
            'lastmod' => $date instanceof DateTime ? $date->format('c') : null,
            'frequency' => $frequency,
            'priority' => $priority,
        ];
    }
}

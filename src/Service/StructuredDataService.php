<?php

declare(strict_types=1);

namespace App\Service;

final class StructuredDataService
{
    public function homepage(): string
    {
        return json_encode([
            '@context' => 'https://schema.org',
            '@type' => 'SportsOrganization',
            'name' => 'Quimper Orientation',
            'legalName' => 'Quimper Orientation',
            'url' => 'https://quimper-orientation.fr',
            'logo' => 'https://quimper-orientation.fr/logo-quimper.webp',
            'sameAs' => [
                'https://www.facebook.com/Quimper-Orientation-Coatarmor-374821382579008',
                'https://www.instagram.com/quimper_orientation/',
                'https://www.strava.com/clubs/474798?oq=quimper',
            ],
            'sport' => 'Course d\'orientation',
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Document;
use App\Repository\DocumentRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

#[AsCommand('app:file:clean', 'Supprime les fichiers inutilisés')]
final class CleanFileCommand extends Command
{
    private array $files = [];

    public function __construct(
        private readonly DocumentRepository $documentRepository,
        private readonly string $rootDir,
        ?string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setHelp('Cette commande vous permet de supprimer tous les fichiers n\'étant plus utilisés.')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Récupération de la liste des fichiers');

        $finder = new Finder();
        // find all files in the current directory
        $finder->files()->in(sprintf('%s/public/files', $this->rootDir));

        foreach ($finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $fileNameWithExtension = $file->getRelativePathname();
            $this->files[$fileNameWithExtension] = $absoluteFilePath;
        }

        $documents = $this->documentRepository->findAll();
        /** @var Document $document */
        foreach ($documents as $document) {
            if (isset($this->files[$document->getFilename()])) {
                unset($this->files[$document->getFilename()]);
            }
        }

        $output->writeln(sprintf('Suppression de %s fichier(s)', count($this->files)));

        foreach ($this->files as $file) {
            $output->writeln(sprintf('Suppression du fichier "%s"', $file));
            unlink($file);
        }

        return Command::SUCCESS;
    }
}

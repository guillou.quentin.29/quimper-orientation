<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\FederalArchiveService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[\Symfony\Component\Console\Attribute\AsCommand('app:federale-archive:update', 'Mise à jour de l\'archive fédérale')]
final class UpdateFederalArchiveCommand extends Command
{
    public function __construct(
        private readonly FederalArchiveService $archiveService,
        private readonly HttpClientInterface $httpClient,
        string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setHelp('Mise à jour la base de données avec l\'archive fédérale datée de ce jour.')
        ;
    }

    /**
     * @inheritDoc
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Mise à jour de l\'archive fédérale');
        $output->writeln('Téléchargement de l\'archive fédérale du jour');

        $response = $this->httpClient->request('GET', 'https://licences.ffcorientation.fr/licencesFFCO-OE2010.csv', [
            'headers' => [
                'Authorization' => 'Token c5eb10281743d1c09e6f66a9bf8a08ff9bb898b8',
            ],
        ]);

        try {
            $file = fopen('/tmp/licencesFFCO-OE2010.csv', 'w+');
            $lines = explode(PHP_EOL, $response->getContent());
            foreach ($lines as $line) {
                fputcsv($file, str_getcsv($line, ';', ''), ';');
            }
            fclose($file);
        } catch (HttpExceptionInterface | TransportExceptionInterface $e) {
            $output->writeln($e->getMessage());
            if (!empty($file)) {
                fclose($file);
            }

            return Command::FAILURE;
        }

        $output->writeln('Import dans la base de données');

        $file = fopen('/tmp/licencesFFCO-OE2010.csv', 'r+');

        $this->archiveService->setFile($file);
        $this->archiveService->update();

        fclose($file);

        $output->writeln('Importations terminées');

        return Command::SUCCESS;
    }
}

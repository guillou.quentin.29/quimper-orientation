<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Event;
use App\Entity\News;
use App\Entity\Result;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[\Symfony\Component\Console\Attribute\AsCommand('app:slug:update', 'Mise à jour des slug')]
final class UpdateSlugCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setHelp('Cette commande vous permet de mettre à jour les slug des différentes entités.')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Mise à jour des slug');

        $output->writeln('Entity: Event');
        $events = $this->entityManager->getRepository(Event::class)->findAll();
        foreach ($events as $event) {
            $event->setSlug(Urlizer::urlize($event->getTitle()));
            $this->entityManager->persist($event);
        }
        $this->entityManager->flush();

        $output->writeln('Entity: News');
        $news = $this->entityManager->getRepository(News::class)->findAll();
        foreach ($news as $new) {
            $new->setSlug(Urlizer::urlize($new->getTitle()));
            $this->entityManager->persist($new);
        }
        $this->entityManager->flush();

        $output->writeln('Entity: Result');
        $results = $this->entityManager->getRepository(Result::class)->findAll();
        foreach ($results as $result) {
            $result->setSlug(Urlizer::urlize($result->getTitle()));
            $this->entityManager->persist($result);
        }
        $this->entityManager->flush();


        $output->writeln('Mise à jour terminé');

        return Command::SUCCESS;
    }
}

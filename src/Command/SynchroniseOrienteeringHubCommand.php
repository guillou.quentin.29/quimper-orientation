<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Circuit;
use App\Entity\Event;
use App\Entity\People;
use App\Entity\Result;
use App\Repository\EventRepository;
use App\Repository\PeopleRepository;
use App\Repository\ResultRepository;
use OrienteeringManager\Config\Enum\AuthorizationLevelEnum;
use OrienteeringManager\Config\Enum\EventRegistrationStatusEnum;
use OrienteeringManager\Config\Enum\EventStatusEnum;
use OrienteeringManager\DataProvider\OrienteeringManagerDataProvider;
use OrienteeringManager\Model\OrienteeringManager\Item\EventApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventLocationApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventRegistrationApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventResultApiItem;
use OrienteeringManager\Model\OrienteeringManager\Item\EventTrackApiItem;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand('app:orienteering-hub:update', 'Mise à jour des données dans Orienteering Hub')]
final class SynchroniseOrienteeringHubCommand extends Command
{
    private SymfonyStyle $io;

    public function __construct(
        private readonly EventRepository $eventRepository,
        private readonly ResultRepository $resultRepository,
        private readonly PeopleRepository $peopleRepository,
        private readonly OrienteeringManagerDataProvider $orienteeringManagerDataProvider,
        string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        $events = $this->eventRepository->findAll();
        foreach ($events as $event) {
            $this->synchronizeEvent($event);
        }

        $results = $this->resultRepository->findAll();
        foreach ($results as $result) {
            $this->synchronizeResult($result);
        }

        $peoples = $this->peopleRepository->findAll();
        foreach ($peoples as $person) {
            $this->synchronizePeople($person);
        }

        return Command::SUCCESS;
    }

    private function synchronizeEvent(Event $event): void
    {
        /** @var EventApiItem $eventApiItem */
        $eventApiItem = null !== $event->getHubId() ?
            $this->orienteeringManagerDataProvider->getItem(new EventApiItem($event->getHubId()))
            : new EventApiItem();

        $eventApiItem->setName($event->getTitle());
        $eventApiItem->setLevel(AuthorizationLevelEnum::UNAUTHORIZED);
        $eventApiItem->setDate($event->getDateBegin());
        $eventApiItem->setEndDate($event->getDateEnd());
        $eventApiItem->setAllowRegistration($event->getAllowEntries());
        $eventApiItem->setRegistrationLimitDate($event->getDateEntries());
        $eventApiItem->setRegistrationDescription($event->getEntryInformations());
        $eventApiItem->setStatus($event->getCancelled() ? EventStatusEnum::CANCELLED : null);
        $eventApiItem->setOrganizer($event->getOrganizer());
        $eventApiItem->setOrganizerWebsite($event->getWebsite());

        $location = $eventApiItem->getLocation() ?? new EventLocationApiItem();
        $location->setName($event->getLocationTitle());
        $location->setDescription($event->getLocationInformation());
        $location->setLatitude(floatval($event->getLatitude()) === 0.0 ? null : floatval($event->getLatitude()));
        $location->setLongitude(floatval($event->getLongitude()) === 0.0 ? null : floatval($event->getLongitude()));
        $eventApiItem->setLocation($location);

        /** @var Circuit $track */
        foreach ($event->getCircuits() as $track) {
            if ($track->getHubId() !== null) {
                continue;
            }

            $eventTrackApiItem = new EventTrackApiItem();
            $eventTrackApiItem->setName($track->getLabel());
            $eventTrackApiItem->setLength($track->getDistance());
            $eventTrackApiItem->setElevationGain($track->getElevation());
            $eventApiItem->addTrack($eventTrackApiItem);

            $track->setHubId(1);
        }

        $eventApiItem = $this->orienteeringManagerDataProvider->save($eventApiItem);

        $event->setHubId($eventApiItem->getId());
        $this->eventRepository->save($event, true);
    }

    private function synchronizeResult(Result $result): void
    {
        if (null === $result->getEvent()?->getHubId()) {
            $this->io->warning(sprintf('Result %s not attached to an imported Event.', $result->getId()));

            return;
        }

        /** @var EventApiItem $eventApiItem */
        $eventApiItem = $this->orienteeringManagerDataProvider->getItem(
            new EventApiItem($result->getEvent()->getHubId())
        );
        $eventResultApiItem = $eventApiItem->getResult() ?? new EventResultApiItem();

        foreach ($result->getFiles() as $file) {
            $this->io->warning($file->getFilename());
        }

        $eventApiItem->setResult($eventResultApiItem);

        $this->orienteeringManagerDataProvider->save($eventApiItem);
    }

    private function synchronizePeople(People $people): void
    {
        if (null === $people->getEvent()?->getHubId()) {
            $this->io->warning(sprintf('Registration %s not attached to an imported Event.', $people->getId()));

            return;
        }

        if ($people->getEvent()->getDateBegin() < new \DateTime('-1 month')) {
            return;
        }

        /** @var EventRegistrationApiItem $eventRegistrationApiItem */
        $eventRegistrationApiItem = null !== $people->getHubId() ?
            $this->orienteeringManagerDataProvider->getItem(new EventRegistrationApiItem($people->getHubId()))
            : new EventRegistrationApiItem();

        $eventRegistrationApiItem->setEvent('/api/events/' . $people->getEvent()->getHubId());
        $eventRegistrationApiItem->setFirstName($people->getFirstName());
        $eventRegistrationApiItem->setLastName($people->getLastName());
        $eventRegistrationApiItem->setCardNumber($people->getSi());
        $eventRegistrationApiItem->setFederationMember(
            $people->getBaseId() !== null ?
                '/api/federation_members/' . $people->getBaseId()
                : null
        );
        $eventRegistrationApiItem->setStatus(EventRegistrationStatusEnum::VALIDATED);
        $eventRegistrationApiItem->setComment(
            $people->getComment()
            . ' circuit : '
            . $people->getCircuit()?->getLabel() ?? ''
        );

        $eventRegistrationApiItem = $this->orienteeringManagerDataProvider->save($eventRegistrationApiItem);

        $people->setHubId($eventRegistrationApiItem->getId());
        $this->peopleRepository->save($people, true);
    }

    private function warning(string $fieldName, int $entityId, mixed $value): void
    {
        $this->io->warning([
            'Field "' . $fieldName . '" is not empty in ' . $entityId,
            $value,
        ]);
    }
}

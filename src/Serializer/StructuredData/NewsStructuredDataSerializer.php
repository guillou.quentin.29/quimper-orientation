<?php

declare(strict_types=1);

namespace App\Serializer\StructuredData;

use App\Entity\News;
use PiWeb\PiCRUD\Serializer\StructuredData\AbstractStructuredDataSerializer;
use PiWeb\PiCRUD\Serializer\StructuredData\StructuredDataSerializerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class NewsStructuredDataSerializer extends AbstractStructuredDataSerializer
{
    public function normalize($object, string $format = null, array $context = []): string
    {
        /** @var News $object */
        return json_encode([
            '@context' => 'https://schema.org',
            '@type' => 'NewsArticle',
            'headline' => $object->getTitle(),
            'datePublished' => $object->getCreateAt()?->format('c'),
            'dateModified' => $object->getUpdateAt()?->format('c'),
            'image' => $object->getImage()?->getName() ?? 'images/default.webp',
            'mainEntityOfPage' => [
                '@type' => 'WebPage',
                "@id" => $this->router->generate(
                    'pi_crud_list',
                    ['type' => 'news'],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
            'author' => [
                '@type' => 'Person',
                'name' => $object->getCreateBy()?->getFirstName() ?? 'Webmaster du Quimper Orientation',
                'url' => $this->router->generate('app_homepage', [], UrlGeneratorInterface::ABSOLUTE_URL),
            ],
            'description' => !empty($object->getMetaDescription()) ?
                $object->getMetaDescription() :
                $object->getTitle(),
            'publisher' => [
                '@type' => 'SportsOrganization',
                'name' => 'Quimper Orientation',
            ],
            'url' => $this->router->generate(
                'pi_crud_show',
                [
                    'type' => 'news',
                    'id' => $object->getId(),
                    'slug' => $object->getSlug(),
                ],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ], JSON_THROW_ON_ERROR);
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof News
            && StructuredDataSerializerInterface::SERIALIZER_FORMAT_STRUCTURED_DATA === $format;
    }
}

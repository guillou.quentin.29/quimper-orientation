<?php

declare(strict_types=1);

namespace App\Serializer\StructuredData;

use App\Entity\Event;
use PiWeb\PiCRUD\Serializer\StructuredData\AbstractStructuredDataSerializer;
use PiWeb\PiCRUD\Serializer\StructuredData\StructuredDataSerializerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class EventStructuredDataSerializer extends AbstractStructuredDataSerializer
{
    public function normalize($object, string $format = null, array $context = []): string
    {
        /** @var Event $object */

        $data = [
            '@context' => 'https://schema.org',
            '@type' => 'SportsEvent',
            'name' => $object->getTitle(),
            'eventStatus' => $object->getCancelled() ?
                'EventCancelled' :
                'EventScheduled',
            'eventAttendanceMode' => 'OfflineEventAttendanceMode',
            'description' => !empty($object->getMetaDescription()) ?
                $object->getMetaDescription() :
                $object->getTitle(),
            'startDate' => $object->getDateBegin()->format('c'),
            'organizer' => [
                '@type' => 'SportsTeam',
                'name' => $object->getOrganizer(),
                'url' => $object->getWebsite() ?? $this->router->generate(
                    'app_homepage',
                    [],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ],
            'url' => $this->router->generate(
                'pi_crud_show',
                [
                    'type' => 'event',
                    'id' => $object->getId(),
                    'slug' => $object->getSlug(),
                ],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
        ];

        if ($object->getDateEnd() > $object->getDateBegin()) {
            $data['endDate'] = $object->getDateEnd()->format('c');
        }

        if (!empty($object->getLatitude()) && !empty($object->getLongitude())) {
            $data['location'] = [
                '@type' => 'Place',
                'name' => $object->getLocationTitle() ?? 'Non défini',
                'geo' => [
                    '@type' => 'GeoCoordinates',
                    'latitude' => $object->getLatitude(),
                    'longitude' => $object->getLongitude(),
                ],
                'address' => [
                    '@type' => 'PostalAddress',
                    'addressLocality' => $object->getLocationTitle() ?? 'Non défini',
                ],
            ];
        } else {
            $data['location'] = [
                '@type' => 'Place',
                'address' => [
                    '@type' => 'PostalAddress',
                    'addressLocality' => $object->getLocationTitle() ?? 'Non défini',
                ],
            ];
        }

        return json_encode($data, JSON_THROW_ON_ERROR);
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Event
            && StructuredDataSerializerInterface::SERIALIZER_FORMAT_STRUCTURED_DATA === $format;
    }
}

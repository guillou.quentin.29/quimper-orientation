<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

trait EventCancelledTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Annuler l'évènement",
     *      form={"class": "order-10"}
     * )
     */
    #[ORM\Column(type: 'boolean')]
    protected bool $cancelled = false;

    public function getCancelled(): ?bool
    {
        return $this->cancelled;
    }

    public function setCancelled(bool $cancelled): self
    {
        $this->cancelled = $cancelled;

        return $this;
    }
}

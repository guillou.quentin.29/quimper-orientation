<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\People;
use App\Entity\Team;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

trait EventEntryTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Activé les inscriptions",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\Column(type: 'boolean')]
    protected bool $allowEntries = true;

    /**
     * @PiCRUD\Property(
     *      label="Informations sur l'inscription",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $entryInformations = null;

    /**
     * @PiCRUD\Property(
     *      label="Date de clôture des inscriptions",
     *      type="datetime",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    protected ?DateTimeInterface $dateEntries = null;

    /**
     * @PiCRUD\Property(
     *      label="Nombre d'inscrit par équipe",
     *      form={"class": "order-5"}
     * )
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $numberPeopleByEntries = null;

    #[ORM\OneToMany(targetEntity: Team::class, cascade: ['persist', 'remove'], mappedBy: 'event')]
    protected Collection $teams;

    #[ORM\OneToMany(targetEntity: People::class, cascade: ['persist', 'remove'], mappedBy: 'event')]
    protected Collection $peoples;

    public function getAllowEntries(): ?bool
    {
        return $this->allowEntries;
    }

    public function setAllowEntries(bool $allowEntries): self
    {
        $this->allowEntries = $allowEntries;

        return $this;
    }

    public function getEntryInformations(): ?string
    {
        return $this->entryInformations;
    }

    public function setEntryInformations(?string $entryInformations): self
    {
        $this->entryInformations = $entryInformations;

        return $this;
    }

    public function getDateEntries(): ?DateTimeInterface
    {
        return $this->dateEntries;
    }

    public function setDateEntries($dateEntries): self
    {
        $this->dateEntries = $dateEntries;

        return $this;
    }

    public function getNumberPeopleByEntries(): ?int
    {
        return $this->numberPeopleByEntries;
    }

    public function setNumberPeopleByEntries($numberPeopleByEntries): self
    {
        $this->numberPeopleByEntries = $numberPeopleByEntries;

        return $this;
    }

    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function getPeoples(): Collection
    {
        return $this->peoples;
    }
}

<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\Document;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PiWeb\PiCRUD\Annotation as PiCRUD;

trait DocumentTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Fichiers",
     *      type="files",
     *      form={"class": "order-6"},
     *      options={"entry_type": "App\Form\DocumentFormType"}
     * )
     */
    #[ORM\ManyToMany(targetEntity: \App\Entity\Document::class, cascade: ['persist', 'remove'])]
    protected Collection $files;

    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function setFiles(Collection $files): self
    {
        $this->files = $files;

        return $this;
    }

    public function addFile(Document $file): self
    {
        $this->files->add($file);

        return $this;
    }

    public function removeFile(Document $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
        }

        return $this;
    }
}

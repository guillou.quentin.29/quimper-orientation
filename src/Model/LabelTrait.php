<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use PiWeb\PiCRUD\Annotation as PiCRUD;

trait LabelTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Libellé",
     *      admin={"class": "font-weight-bold"},
     *      form={"class": "order-1"}
     * )
     */
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $label = null;

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    #[Pure]
    public function __toString(): string
    {
        return $this->getLabel() ?? '';
    }
}

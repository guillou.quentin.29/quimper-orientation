<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use PiWeb\PiCRUD\Annotation as PiCRUD;

trait UserNameTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Prénom",
     *      admin={"class": "d-none d-lg-table-cell"},
     *      form={"class": "order-1"}
     * )
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $firstName = null;

    /**
     * @PiCRUD\Property(
     *      label="Nom",
     *      admin={"class": "d-none d-lg-table-cell"},
     *      form={"class": "order-1"}
     * )
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $lastName = null;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}

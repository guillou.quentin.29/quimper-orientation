<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

trait PromoteTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Promu",
     *      type="checkbox",
     *      form={"class": "order-12"}
     * )
     */
    #[ORM\Column(name: 'promote', type: 'boolean', nullable: true)]
    protected bool $promote = true;

    public function isPromote(): bool
    {
        return $this->promote;
    }

    public function setPromote(bool $promote): self
    {
        $this->promote = $promote;

        return $this;
    }
}

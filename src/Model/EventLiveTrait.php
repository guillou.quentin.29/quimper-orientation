<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use PiWeb\PiCRUD\Annotation as PiCRUD;

trait EventLiveTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Résultats live (à activer le jour de la course)",
     *      type="checkbox",
     *      form={"class": "order-12"}
     * )
     */
    #[ORM\Column(name: 'live', type: 'boolean', nullable: true)]
    protected ?bool $live = false;

    public function isLive(): ?bool
    {
        return $this->live;
    }

    public function setLive(?bool $live): self
    {
        $this->live = $live;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\Event;

trait EventReferenceTrait
{
    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(Event $event): self
    {
        $this->event = $event;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use PiWeb\PiCRUD\Annotation as PiCRUD;

trait PrivateTrait
{
    /**
     * @PiCRUD\Property(
     *      label="Privé",
     *      type="checkbox",
     *      admin={"class": "d-none d-md-table-cell"},
     *      form={"class": "order-12"}
     * )
     */
    #[ORM\Column(name: 'private', type: 'boolean', nullable: true)]
    protected ?bool $private = false;

    public function isPrivate(): ?bool
    {
        return $this->private;
    }

    public function setPrivate(?bool $private): self
    {
        $this->private = $private;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use PiWeb\PiCRUD\Annotation as PiCRUD;

trait ImageTrait
{
    /**
     * @Vich\UploadableField(
     *     mapping="content_image",
     *     fileNameProperty="image.name",
     *     size="image.size",
     *     mimeType="image.mimeType",
     *     originalName="image.originalName",
     *     dimensions="image.dimensions"
     * )
     * @PiCRUD\Property(
     *      label="Image",
     *      type="image",
     *      form={"class": "order-1"}
     * )
     */
    protected ?File $imageFile = null;

    #[ORM\Embedded(class: EmbeddedFile::class)]
    protected EmbeddedFile $image;

    public function setImageFile(?File $imageFile = null): self
    {
        $this->imageFile = $imageFile;

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImage(EmbeddedFile $image): void
    {
        $this->image = $image;
    }

    public function getImage(): ?EmbeddedFile
    {
        return $this->image;
    }
}

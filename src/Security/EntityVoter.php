<?php

declare(strict_types=1);

namespace App\Security;

use PiWeb\PiCRUD\Security\AbstractVoter;

abstract class EntityVoter extends AbstractVoter
{
    protected function canAdmin($subject, $user): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    protected function canAdd($subject, $user): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    protected function canEdit($subject, $user): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }

    protected function canDelete($subject, $user): bool
    {
        return $this->security->isGranted('ROLE_ADMIN');
    }
}

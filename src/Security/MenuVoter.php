<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Menu;

final class MenuVoter extends EntityVoter
{
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!$subject instanceof Menu && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'menu')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }

    protected function canShow($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_MEMBER')) {
            return true;
        }

        return !$entity->isPrivate();
    }
}

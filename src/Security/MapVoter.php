<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Map;

final class MapVoter extends ContentVoter
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof Map && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'map')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }
}

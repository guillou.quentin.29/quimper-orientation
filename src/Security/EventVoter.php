<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Event;

final class EventVoter extends ContentVoter
{
    public const REGISTER = 'register';

    protected function supports(string $attribute, $subject): bool
    {
        if (
            !$subject instanceof Event
            && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN, self::REGISTER]) && $subject === 'event')
        ) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }

    protected function canRegister($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        if ($entity->getAllowEntries() && $entity->getDateEntries()->format('U') > date('U')) {
            return true;
        }

        return false;
    }

    protected function canClone($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_WEBMASTER')) {
            return true;
        }

        return false;
    }
}

<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\People;
use App\Entity\User;

final class PeopleVoter extends EntityVoter
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof People && !(in_array($attribute, [self::ADD, self::EDIT, self::LIST, self::ADMIN]) && $subject === 'people')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }

    protected function canAdd($entity, $user): bool
    {
        return true;
    }

    protected function canEdit($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY') && $user->getBase() === $entity->getBase()) {
            return true;
        }

        if ($user instanceof User && $user === $entity->getCreateBy()) {
            return true;
        }

        return false;
    }

    protected function canDelete($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY') && $user->getBase() === $entity->getBase()) {
            return true;
        }

        if ($user instanceof User && $user === $entity->getCreateBy()) {
            return true;
        }

        return false;
    }
}

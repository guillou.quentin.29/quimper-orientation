<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;

final class UserVoter extends EntityVoter
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof User && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'user')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }

    protected function canAdd($subject, $user): bool
    {
        return false;
    }

    protected function canEdit($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return false;
    }

    protected function canDelete($map, $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return false;
    }
}

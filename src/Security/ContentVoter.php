<?php

declare(strict_types=1);

namespace App\Security;

use PiWeb\PiCRUD\Security\AbstractVoter;

abstract class ContentVoter extends AbstractVoter
{
    protected function canAdmin($subject, $user): bool
    {
        return $this->security->isGranted('ROLE_WEBMASTER');
    }

    protected function canShow($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_MEMBER')) {
            return true;
        }

        return !$entity->isPrivate();
    }

    protected function canList($entity, $user): bool
    {
        return true;
    }

    protected function canAdd($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_WEBMASTER')) {
            return true;
        }

        return false;
    }

    protected function canEdit($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_WEBMASTER')) {
            return true;
        }

        return false;
    }

    protected function canDelete($map, $user): bool
    {
        if ($this->security->isGranted('ROLE_WEBMASTER')) {
            return true;
        }

        return false;
    }
}

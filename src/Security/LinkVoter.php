<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Link;

final class LinkVoter extends EntityVoter
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof Link && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'link')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }

    protected function canShow($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_MEMBER')) {
            return true;
        }

        return !$entity->isPrivate();
    }
}

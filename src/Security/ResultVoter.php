<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Result;

final class ResultVoter extends ContentVoter
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof Result && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'result')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }
}

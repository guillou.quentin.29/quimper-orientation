<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\News;

final class NewsVoter extends ContentVoter
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof News && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'news')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }
}

<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Format;

final class FormatVoter extends EntityVoter
{
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof Format && !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'format')) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }
}

<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Team;
use App\Entity\User;

final class TeamVoter extends EntityVoter
{
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (
            !$subject instanceof Team &&
            !(in_array($attribute, [self::ADD, self::LIST, self::ADMIN]) && $subject === 'team')
        ) {
            return false;
        }

        return parent::supports($attribute, $subject);
    }

    protected function canAdd($entity, $user): bool
    {
        return true;
    }

    protected function canEdit($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        if ($user instanceof User && $user->getBase() === $entity->getBase()) {
            return true;
        }

        if ($user instanceof User && $user === $entity->getCreateBy()) {
            return true;
        }

        return false;
    }

    protected function canDelete($entity, $user): bool
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        if ($user instanceof User && $user->getBase() === $entity->getBase()) {
            return true;
        }

        if ($user instanceof User && $user === $entity->getCreateBy()) {
            return true;
        }

        return false;
    }
}

#!/bin/bash

echo "Starting application update";

ENVIRONMENT=$1
VERSION=$2

echo "Step 1 - Loading environment files";
cp ../config/.env .env

echo "Step 2 - Creating symbolic link to public folders";
ln -s /var/www/orientation/quimper-orientation/$ENVIRONMENT/content/images public/images
ln -s /var/www/orientation/quimper-orientation/$ENVIRONMENT/content/files public/files
ln -s /var/www/orientation/quimper-orientation/$ENVIRONMENT/content/live public/live
ln -S googlef9d1e702e821166b.html /var/www/orientation/quimper-orientation/$ENVIRONMENT/content/googlef9d1e702e821166b.html
mkdir var/
mkdir var/cache
mkdir var/log

echo "Step 3 - Updating permissions";
chmod -R +x bin/
chmod 777 -R var

echo "Step 4 - Updating Ckeditor app CSS";
cd public
APP_CSS_FILENAME="$(find ./build -name 'app.*.css')"
cd ..
sed -i "s@/build/app.css@${APP_CSS_FILENAME}@g" config/packages/fos_ckeditor.yaml

echo "Step 5 - Updating database schema";
bin/console doctrine:migration:migrate -n

echo "Step 6 - Installing assets"
bin/console ckeditor:install
bin/console elfinder:install
bin/console assets:install

echo "Step 7 - Switching application to current"
cd ..
unlink current
ln -s quimper-orientation-$VERSION current

echo "Step 8 - Updating permissions"
chmod 777 -R current/var

echo "Application successfully updated";

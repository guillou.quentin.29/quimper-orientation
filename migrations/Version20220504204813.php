<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220504204813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event ADD meta_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE news ADD meta_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE result ADD meta_description VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event DROP meta_description');
        $this->addSql('ALTER TABLE news DROP meta_description');
        $this->addSql('ALTER TABLE result DROP meta_description');
    }
}

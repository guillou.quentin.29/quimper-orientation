<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220424124551 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE event_associated (event_source INT NOT NULL, event_target INT NOT NULL, INDEX IDX_8B3B89F96D130821 (event_source), INDEX IDX_8B3B89F974F658AE (event_target), PRIMARY KEY(event_source, event_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event_associated ADD CONSTRAINT FK_8B3B89F96D130821 FOREIGN KEY (event_source) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_associated ADD CONSTRAINT FK_8B3B89F974F658AE FOREIGN KEY (event_target) REFERENCES event (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE event_associated');
    }
}

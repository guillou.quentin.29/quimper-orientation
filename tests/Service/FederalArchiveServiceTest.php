<?php

declare(strict_types=1);

namespace Service;

use App\Service\FederalArchiveService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class FederalArchiveServiceTest extends WebTestCase
{
    private EntityManagerInterface $entityManager;
    private FederalArchiveService $federalArchiveService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->createMock(EntityManagerInterface::class);

        $this->federalArchiveService = new FederalArchiveService($this->entityManager);
    }


    public function testUpdateWithoutSetFile(): void
    {
        $this->expectException(BadRequestException::class);
        $this->federalArchiveService->update();
    }
}

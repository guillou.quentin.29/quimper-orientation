const $ = require('jquery');

$(function() {
    $('.modal-confirm').on('show.bs.modal', function(e) {
        $(this).find('a').attr('href', $(e.relatedTarget).data('href'));
    });
})

import '../css/calendar.scss';

const jQuery = require('jquery');
const $ = jQuery;

$(function() {
    ;(function ($, window, document, undefined) {

        "use strict";

        // Create the defaults once
        const pluginName = "simpleCalendar",
            defaults = {
                months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'], //string of months starting from january
                days: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'], //string of days starting from sunday
                displayYear: true, // display year in header
                fixedStartDay: true, // Week begin always by monday
                displayEvent: true, // display existing event
                disableEventDetails: false, // disable showing event details
                disableEmptyDetails: false, // disable showing empty date details
                events: [], // List of event
                onMonthChange: function (month, year) {
                }, // Callback on month change
            };

        // The actual plugin constructor
        function Plugin(element, options) {
            this.element = element;
            this.settings = $.extend({}, defaults, options);
            this._defaults = defaults;
            this._name = pluginName;
            this.currentDate = new Date();
            this.init();
        }

        // Avoid Plugin.prototype conflicts
        $.extend(Plugin.prototype, {
            init: function () {
                const container = $(this.element);
                const todayDate = this.currentDate;

                const calendar = $('<div class="calendar"></div>');
                const header = $('<header>' +
                    '<h2 class="month text-center"></h2>' +
                    '<span class="btn btn-prev"></span>' +
                    '<span class="btn btn-next"></span>' +
                    '</header>');

                const _this = this;

                $.ajax('/event/json', {
                    success: function (data) {
                        $.each(JSON.parse(data), function(index, event) {
                            _this.settings.events.push({
                                id: event.id,
                                startDate: new Date(event.dateBegin),
                                endDate: event.dateEnd ? new Date(event.dateEnd) : new Date(event.dateBegin),
                                summary: event.title,
                                slug: event.slug
                            });
                        });

                        _this.buildCalendar(todayDate, calendar);
                        container.html(calendar);

                        _this.bindEvents();
                    }
                });

                _this.updateHeader(todayDate, header);
                calendar.append(header);

                _this.buildCalendar(todayDate, calendar);
                container.html(calendar);
            },

            //Update the current month header
            updateHeader: function (date, header) {
                let monthText = this.settings.months[date.getMonth()];
                monthText += this.settings.displayYear ? ' <div class="year">' + date.getFullYear() : '</div>';
                header.find('.month').html(monthText);
            },

            //Build calendar of a month from date
            buildCalendar: function (fromDate, calendar) {
                const plugin = this;

                calendar.find('table').remove();

                const body = $('<table class="w-100"></table>');
                const thead = $('<thead></thead>');
                const tbody = $('<tbody></tbody>');

                //Header day in a week ( (1 to 8) % 7 to start the week by monday)
                for (var i = 1; i <= this.settings.days.length; i++) {
                    thead.append($('<td>' + this.settings.days[i % 7].substring(0, 3) + '</td>'));
                }

                //setting current year and month
                const y = fromDate.getFullYear(), m = fromDate.getMonth();

                //first day of the month
                const firstDay = new Date(y, m, 1);
                //If not monday set to previous monday
                while (firstDay.getDay() !== 1) {
                    firstDay.setDate(firstDay.getDate() - 1);
                }
                //last day of the month
                const lastDay = new Date(y, m + 1, 0);
                //If not sunday set to next sunday
                while (lastDay.getDay() !== 0) {
                    lastDay.setDate(lastDay.getDate() + 1);
                }

                //For firstDay to lastDay
                for (const day = firstDay; day <= lastDay; day.setDate(day.getDate())) {
                    const tr = $('<tr></tr>');
                    //For each row
                    for (var i = 0; i < 7; i++) {
                        const td = $('<td><div class="day text-center" data-date="' + day.toISOString() + '">' + day.getDate() + '</div></td>');

                        //if today is this day
                        if (day.toDateString() === (new Date).toDateString()) {
                            td.find(".day").addClass("today");
                        }

                        //if day is not in this month
                        if (day.getMonth() !== fromDate.getMonth()) {
                            td.find(".day").addClass("wrong-month");
                        }

                        // filter today's events
                        const todayEvents = plugin.getDateEvents(day);

                        if (todayEvents.length && plugin.settings.displayEvent) {
                            td.find(".day").addClass(plugin.settings.disableEventDetails ? "has-event disabled" : "has-event");
                        } else {
                            td.find(".day").addClass(plugin.settings.disableEmptyDetails ? "disabled" : "");
                        }

                        tr.append(td);
                        day.setDate(day.getDate() + 1);
                    }
                    tbody.append(tr);
                }

                body.append(thead);
                body.append(tbody);

                const eventContainer = $('<div class="event-container">' +
                    '<div class="close"></div>' +
                    '<h3 class="text-white"></h3>' +
                    '<div class="event-wrapper"></div></div>'
                );

                calendar.append(body);
                calendar.append(eventContainer);
            },
            changeMonth: function (value) {
                this.currentDate.setMonth(this.currentDate.getMonth() + value);
                this.buildCalendar(this.currentDate, $(this.element).find('.calendar'));
                this.updateHeader(this.currentDate, $(this.element).find('.calendar header'));
                this.settings.onMonthChange(this.currentDate.getMonth(), this.currentDate.getFullYear())
            },
            //Init global events listeners
            bindEvents: function () {
                const plugin = this;

                //Click previous month
                $(plugin.element).on('click', '.btn-prev', function () {
                    plugin.changeMonth(-1)
                });

                //Click next month
                $(plugin.element).on('click', '.btn-next', function () {
                    plugin.changeMonth(1)
                });

                //Binding day event
                $(plugin.element).on('click', '.day', function (e) {
                    const date = new Date($(this).data('date'));
                    const events = plugin.getDateEvents(date);
                    if (!$(this).hasClass('disabled')) {
                        plugin.fillUp(e.pageX, e.pageY);
                        plugin.displayEvents(date, events);
                    }
                });

                //Binding event container close
                $(plugin.element).on('click', '.event-container .close', function (e) {
                    plugin.empty(e.pageX, e.pageY);
                });
            },
            displayEvents: function (date, events) {
                const plugin = this;
                const container = $(this.element).find('.event-wrapper');
                container.html('');

                $(this.element).find('h3').html(plugin.formatDateEvent(date, date));

                events.forEach(function (event) {
                    const $event = $('' +
                        '<div class="event">' +
                        ' <h5>' + event.summary + '</h5>' +
                        ' <a href="/event/' + event.id + '-' + event.slug + '.html" class="text-primary stretched-link">Plus d\'informations</a>' +
                        '</div>');
                    container.append($event);
                });

                if (events.length === 0) {
                    container.html('Aucun évènement');
                }
            },
            //Small effect to fillup a container
            fillUp: function (x, y) {
                const plugin = this;
                const elem = $(plugin.element);
                const elemOffset = elem.offset();

                const filler = $('<div class="filler" style=""></div>');
                filler.css("left", x - elemOffset.left);
                filler.css("top", y - elemOffset.top);

                elem.find('.calendar').append(filler);

                filler.animate({
                    width: "100%",
                    height: "100%"
                }, 500, function () {
                    elem.find('.event-container').show();
                    filler.hide();
                });
            },
            //Small effect to empty a container
            empty: function (x, y) {
                const plugin = this;
                const elem = $(plugin.element);

                const filler = elem.find('.filler');
                filler.css("width", "100%");
                filler.css("height", "100%");

                filler.show();

                elem.find('.event-container').hide().find('.event').remove();

                filler.animate({
                    width: "0%",
                    height: "0%"
                }, 500, function () {
                    filler.remove();
                });
            },
            getDateEvents: function (d) {
                const plugin = this;
                return plugin.settings.events.filter(function (event) {
                    return plugin.isDayBetween(d, event.startDate, event.endDate);
                });
            },
            isDayBetween: function (d, dStart, dEnd) {
                dStart.setHours(0,0,0);
                dEnd.setHours(23,59,59,999);
                d.setHours(12,0,0);

                return dStart <= d && d <= dEnd;
            },
            formatDateEvent: function (dateStart, dateEnd) {
                let formatted = '';
                formatted += this.settings.days[dateStart.getDay()] + ' ' + dateStart.getDate() + ' ' + this.settings.months[dateStart.getMonth()];

                if (dateEnd.getDate() !== dateStart.getDate()) {
                    formatted += ' to ' + dateEnd.getDate() + ' ' + this.settings.months[dateEnd.getMonth()].substring(0, 3)
                }
                return formatted;
            }
        });

        // A really lightweight plugin wrapper around the constructor,
        // preventing against multiple instantiations
        $.fn[pluginName] = function (options) {
            return this.each(function () {
                if (!$.data(this, "plugin_" + pluginName)) {
                    $.data(this, "plugin_" + pluginName, new Plugin(this, options));
                }
            });
        };

    })(jQuery, window, document);

    $('#calendar').simpleCalendar({events: []});
});

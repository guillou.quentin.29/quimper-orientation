import '../css/app.scss';

const $ = require('jquery');

require('bootstrap/js/dist/carousel');
require('bootstrap/js/dist/dropdown');
require('bootstrap/js/dist/modal');
require('bootstrap/js/dist/collapse');
require('bootstrap/js/dist/alert');
require('bootstrap/js/dist/util');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/fontawesome');

$(function() {
    let body = $('body');

    // Gestion du menu mobile
    let menuWrapper = $('#mobile-nav-content');
    let menuBtn = $('#menu-btn');
    menuBtn.click(function() {
        menuWrapper.show();
        body.css({
            overflow: 'hidden',
            height: '100%'
        });
    });
    let closeMenuBtn = $('#close-menu-btn');
    closeMenuBtn.click(function() {
        menuWrapper.hide();
        body.css({
            overflow: 'auto',
            height: 'auto'
        });
    });

    // Gestion de l'affichage du thème par defaut
    let div_prefers_color_scheme = $('#prefers-color-scheme');
    if (localStorage.getItem('prefers-color-scheme') === 'dark') {
        body.addClass('dark-theme');
        div_prefers_color_scheme.data('theme', 1);
        div_prefers_color_scheme.find('span').html("Passer en thème clair");
    } else {
        div_prefers_color_scheme.find('span').html("Passer en thème sombre");
        div_prefers_color_scheme.data('theme', 0);
    }

    div_prefers_color_scheme.on('click', function() {
        if($(this).data('theme') === 0) {
            localStorage.setItem('prefers-color-scheme', 'dark');
            body.addClass('dark-theme');
            $(this).find('span').html("Passer en thème clair");
            $(this).data('theme', 1);
        } else {
            localStorage.setItem('prefers-color-scheme', 'default');
            body.removeClass('dark-theme');
            $(this).find('span').html("Passer en thème sombre");
            $(this).data('theme', 0);
        }
    });
});

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-180192518-1', 'auto');

let logDownloadFile = function(title, label) {
    ga('send', {
        hitType: 'event',
        eventCategory: title,
        eventAction: 'Téléchargement de fichier',
        eventLabel: label
    });
};

// Téléchargement des cartes
let downloadMapFileBtn = document.querySelectorAll('.btn-download-map-file')
downloadMapFileBtn.forEach(el => el.addEventListener('click', function() {
    logDownloadFile('Parcours permanents', el.title);
}));

// Téléchargement des fichiers d'événements
let downloadEventFileBtn = document.querySelectorAll('.btn-download-event-file')
downloadEventFileBtn.forEach(el => el.addEventListener('click', function() {
    logDownloadFile('Événement', el.title);
}));

// Téléchargement des fichiers résultats
let downloadResultFileBtn = document.querySelectorAll('.btn-download-result-file')
downloadResultFileBtn.forEach(el => el.addEventListener('click', function() {
    logDownloadFile('Résultat', el.title);
}));

// Téléchargement des fichiers actualités
let downloadNewsFileBtn = document.querySelectorAll('.btn-download-news-file')
downloadNewsFileBtn.forEach(el => el.addEventListener('click', function() {
    logDownloadFile('Actualités', el.title);
}));
const $ = require('jquery');

$(function() {
    $('ul.collections').each(function() {
        const $collectionHolder = $(this);

        const $addTagButton = $collectionHolder.find('button.collection-add');

        $collectionHolder.find('li').children().each(function() {
            addTagFormDeleteLink($collectionHolder, $(this));
        });

        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addTagButton.on('click', function() {
            addTagForm($collectionHolder, $addTagButton);
        });
    });

    function addTagForm($collectionHolder, $addTagButton) {
        const prototype = $collectionHolder.data('prototype');

        const index = $collectionHolder.data('index');

        let newForm = prototype;
        newForm = newForm.replace(/__name__/g, index);

        $collectionHolder.data('index', index + 1);

        const $newFormLi = $('<li></li>').append(newForm);
        const $newFormDiv = $newFormLi.children();
        $newFormDiv.addClass('form-inline mb-4');
        $newFormDiv.children().addClass('form-group pr-4');

        $addTagButton.before($newFormLi);
        addTagFormDeleteLink($collectionHolder, $newFormDiv);
    }
    function addTagFormDeleteLink($collectionHolder, $tagFormLi) {
        const $removeFormButton = $($collectionHolder.data('delete'));
        $tagFormLi.append($removeFormButton);

        $removeFormButton.on('click', function() {
            $tagFormLi.remove();
        });
    }
})

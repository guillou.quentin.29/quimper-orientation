const $ = require('jquery');

$(function() {
    let loading = [];
    let width = $(window).width();

    let wrapper = $('.infinite-scroll-x');
    wrapper.scroll(function() {
        let type = wrapper.attr('type');
        let scrollWidth = wrapper.get(0).scrollWidth;
        if (!loading[type] && scrollWidth - wrapper.scrollLeft() - width < 200) {
            loading[type] = true;
            loadContent($(this), type, wrapper.attr('href'), parseInt(wrapper.attr('start')));
        }
    });

    let loadContent = function (div, type, url, start) {
        $.ajax({
            type: 'GET',
            url: url,
            data: {
                'start': start
            },
            dataType: 'json',
            success: function (response) {
                if (response) {
                    div.attr('start', start + 4);
                    div.append(response);
                    loading[type] = false;
                }
            },
        });
    };
});
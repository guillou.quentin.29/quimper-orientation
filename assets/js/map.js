import '../css/map.scss';

const $ = require('jquery');

require('leaflet');

$(function() {
    const map = L.map('map');
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        minZoom: 1,
        maxZoom: 25
    }).addTo(map);

    let latLngs = [];

    let mapWrapper = $('#map');
    mapWrapper.each(function() {
        if ('default' === $(this).attr('type')) {
            let coordinates = [$(this).data('lat'), $(this).data('lon')];
            map.setView(coordinates, 11);
            L.marker(coordinates, { icon: L.icon({iconUrl: '../images/balise.webp'})}).addTo(map);
        } else {
            $('.coordinates').each(function () {
                let coordinates = [$(this).data('lat'), $(this).data('lon')];
                let title = $(this).data('title');
                let url = '';
                if ('' !== $(this).data('url')) {
                    url = '<br/><a href="' + $(this).data('url') + '">Télécharger la carte</a>';
                }

                let marker = L.marker(coordinates, { icon: L.icon({iconUrl: '../images/balise.webp'}), alt: title});
                marker.addTo(map);

                let popup = L.popup()
                    .setContent('<p>' + title + url + '</p>')
                    .openOn(map);
                marker.bindPopup(popup);

                latLngs.push(coordinates);
            });
        }
    });

    let nextLabel = 'Masquer la carte';
    $('#map-button').click(function () {
        let currentLabel = $(this).text();
        $(this).text(nextLabel);
        nextLabel = currentLabel;

        mapWrapper.toggleClass('d-none');

        let bounds = new L.LatLngBounds(latLngs);
        console.log(latLngs);
        map.fitBounds(bounds);
    });
})

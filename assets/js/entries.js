const $ = require('jquery');

$(function() {
    $(':checkbox').change(function() {
        if ($(':checkbox:checked').length > 0) {
            $('.form_validation').addClass('fixed-bottom');
        } else {
            $('.form_validation').removeClass('fixed-bottom');
        }
    });
});
